test: drop
	@mocha -t 100000

install:
	@sudo apt-add-repository ppa:richarvey/nodejs -y
	@sudo apt-add-repository ppa:nginx/stable -y
	@sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
	@sudo echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list
	@sudo apt-get update
	@sudo apt-get install nodejs -y;
	@sudo apt-get install nginx -y;
	@sudo apt-get install mongodb-org -y;
	@sudo apt-get install imagemagick

drop:
	@node test/helper/drop.js

.PHONY: test drop clean
