require('./helper/require.js');
var login = require('./user').login;

describe('upload', function() {
    var user;
    before(function (done) {
        login(function (err, res) {
            if (err) done(err);
            user = res;
            done();
        })
    });
    it('upload', function (done) {
        createBase64(function (err, res) {
            if (err) return done(err);

            request(host)
                .post('/file')
                .send({
                    file: res
                })
                .set('cookie', user.headers['set-cookie'])
                .expect(200)
                .end(function(err, res) {
                    if (err) return done(err);
                    res.body.should.have.property('file');
                    debugger
                    request(host)
                        .get('/files/images/uploads/' + res.body.file)
                        .expect(200)
                        .end(function(err, res) {
                          if (err) return done(err);
                          done();
                        })

                })
        })
    });
});

function createBase64(cb) {
    var images = fs.readdirSync( __dirname + '/images/');
    images = images.filter(function(e){
        if (path.extname(e).indexOf('jpg')) return e;
    });

    var image = images[Math.floor(Math.random() * images.length)];

    fs.readFile(__dirname + '/images/' + image, function(err, buffer) {
        if (err) cb(err);

        cb(null, buffer.toString('base64'))
    })
}
