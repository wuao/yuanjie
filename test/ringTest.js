// global.root = __dirname + '/../';
// var should = require('chai').should();
// var fs = require('fs');
// var mongoose = require('mongoose');
// var config = require(root + '/config/config');
// var db = mongoose.connect(config.db.uri);
// db.connection.on('error', console.error.bind(console, 'mongodb connection error:'));

// var generate = require(root + '/util/ringCodeGenerate').generate;
// var save = require(root + '/util/ringCodeGenerate').save;
// var parse = require(root + '/util/ringCodeGenerate').parse;


// describe('ring', function() {
//     var count = 100;
//     var half = count / 2;
    
//     it('generate', function (done) {
//         generate(count, function (err, maleRings, femaleRings) {
//             if (err) done(err);

//             maleRings.should.have.length(half)
//             femaleRings.should.have.length(half)
            
//             var maleCount = 0;
//             maleRings.forEach(function (v) {
//                 maleCount += v.number;
//             });
//             var femaleCount = 0;
//             maleRings.forEach(function (v) {
//                 femaleCount += v.number;
//             });

//             maleCount.should.eql( (half+1) * (half/2) );
//             femaleCount.should.eql( (half+1) * (half/2) );
//             done()
//         })
//     });

//     describe('file operator', function() {
//         it('convert data to file', function(done) {
//             generate(10, function (err, maleRings, femaleRings) {
//                 if (err) done(err);
                
//                 save(maleRings, 'male.txt', function (err) {
//                     if (err) done(err);
//                     fs.existsSync(root + '/files/male.txt').should.be.true;
//                     done()
//                 })
//             })
//         });

//         it('parse file to data', function(done) {
//             generate(20, function (err, maleRings, femaleRings) {
//                 if (err) done(err);
                
//                 save(femaleRings, 'female.txt', function (err) {
//                     if (err) done(err);
//                     fs.existsSync(root + '/files/female.txt').should.be.true;
//                     parse(root + '/files/female.txt', function (err, data) {
//                         data.should.have.length(10);
//                         done()
//                     });

//                 })
//             })
//         });
//     });
    
// });
