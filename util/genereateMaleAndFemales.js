global.root = __dirname + '/../';
var mongoose = require('mongoose');
var config = require(root + '/config/config');
var db = mongoose.connect(config.db.uri);
db.connection.on('error', console.error.bind(console, 'mongodb connection error:'));

var generate = require('./ringCodeGenerate.js').generate;
var save = require('./ringCodeGenerate.js').save;
var async = require('async');

generate(10000, function (err, male, female) {
    async.parallel([
        function (n) {
            save(male, 'male.txt', function (err) {
                n()
            })
        },
        function (n) {
            save(female, 'female.txt', function (err) {
                n()
                console.log('ok')
            })
        }
    ]);
})