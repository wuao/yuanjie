// Generate ring data
var Ring = require(root + '/model/ringModel').Ring;
var shortId = require('shortid');
var async = require('async');

function generate(count, cb) {
    var half = count / 2;
    async.parallel({
        male: function(n) {
            async.times(half, function(n, next) {
                Ring.create({
                    number: n + 1,
                    gender: 'male',
                    code: shortId.generate()
                }, function(err, ring) {
                    next(err, ring);
                })
            }, function(err, rings) {
                if (err) console.log(err);
                n(err, rings);
            })
        },

        female: function(n) {
            async.times(half, function(n, next) {
                Ring.create({
                    number: n + 1,
                    gender: 'female',
                    code: shortId.generate()
                }, function(err, ring) {
                    next(err, ring);
                })
            }, function(err, rings) {
                if (err) console.log(err);
                n(err, rings);
            })
        },
    }, function(err, results) {
        cb(err, results.male, results.female);
    })
}

// Convert Data to file
var fs = require('fs');

function save (data, filename, cb) {
    fs.writeFile( root + '/files/' + filename, JSON.stringify(data, function (k,v) {
        if (k === '_id' || k === '__v') return undefined;
        return v;
    }, '  '), function (err) {
        cb(err);
    })
}

function parse (filename, cb) {
    var content = fs.readFileSync(filename);
    cb(null, JSON.parse(content));
}

module.exports.generate = generate;
module.exports.save = save;
module.exports.parse = parse;