var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RingSchema = new Schema({
    number: { type: Number, required: true },
    gender: { type: String, required: true, enum: ['male', 'female'] },
    code: { type: String, required: true }
});

module.exports.Ring = mongoose.model('Ring', RingSchema);