var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId; 

var NewsSchema = new Schema({
    title: { type: String, required: true },
    content: { type: String, required: true }
});

NewsSchema.plugin(require('mongoose-timestamp'));

module.exports.News = mongoose.model('News', NewsSchema);