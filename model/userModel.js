var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongoose_user = require('mongoose-user');
var uniqueValidator = require('mongoose-unique-validator');

var UserSchema = new Schema({
    username: { type: String, required: true, unique: true },
    hashed_password: { type: String, required: true },
    nickname: { type: String, required: true },
    
    // not required
    gender: { type: String, enum: ['male', 'female'] },
    phone: { type: String },
    address: { type: String },
    number: { type: Number },
    married: { type: String, enum: ['married', 'not married'] },
    avatar: { type: String },
    realname: { type: String },
});

UserSchema.plugin(mongoose_user);
UserSchema.plugin(uniqueValidator, { message: 'Error, expected {PATH} to be unique.' });

module.exports.User = mongoose.model('User', UserSchema);