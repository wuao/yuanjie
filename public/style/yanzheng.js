/***        *功能：隐藏和显示div        *参数divDisplay：html标签id        ***/

function click_a(divDisplay) {
    var last_div;
    var obj = document.getElementById(divDisplay);
    if (obj.style.display != "block") {
        if (last_div) last_div.style.display = "none";
        obj.style.display = "block";
        last_div = obj;
    }
    else {
        obj.style.display = "none";
        last_div = null;
    }
}
