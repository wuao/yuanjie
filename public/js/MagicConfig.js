define(function(require, exports, module) {
	// TODO configuration should be filtered by profile setting.
	var magicConfig = {
		cmsBase : "http://games.hoolai.com/cms",
		// magicBase : "http://192.168.1.206:8181/order",
		// magicBase: "http://www.lepao.com:8080/lepao-order-web",
		// magicBase: "http://119.254.66.181:8090",
//		 magicBase: "http://192.168.50.159:8080/lepao-order-web", //声明：不要把自己本地的URL提交到svn
//		magicBase: "http://www.lepao.com:8090/order", //线上测试
		magicBase: "http://www.lepao.com/", //声明：不要把自己本地的URL提交到svn
		imageUploadUrl:"http://app1101081259.qzone.qzoneapp.com/magic/upload/uploadImage",
		// FIXME 现在验证码 分属别的服务器
		// captchaBase:"http://192.168.50.160:8080/portal"
		// captchaBase:"http://192.168.1.206:8181/order"
		targetUrl : {
			cart : "cart.html",
			userBuyInfo : "userbuyinfo.html",
			orderDetail : "order-details.html",
			preOrder : "pre-order.html",
			myOrder : "/my-order.html",
			paymentType : "payment-type.html",
			success : "success.html",
			paymentHint : "payment-hint.html",
			productDetail : "product_details.html",
			RestPsw : "js/view/RestPswTpl.html",
			indexPage : "index.html",
			personalPage : "user-center.html"
		}
	};
	module.exports = magicConfig;

});
