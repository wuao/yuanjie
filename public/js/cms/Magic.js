define(function(require, exports, module) {
	var $ = require('jquery');
	var Logger = require('../util/Log');
	var xaj = require('util/XAjax.js');
	var magicConstant = require('MagicConstant');
	var logger = new Logger();
	var REG_TYPE_PHONE = 1;
	var REG_TYPE_MAIL = 2;
	//TODO configuration should be filtered by profile setting.
	var DEFAULT_DOMAIN = "http://127.0.0.1:88/wordpress/";
	module.exports = Magic;

	/**
	 * 请参考WordPress JSON API插件
	 */
	function Magic(domain) {
		if (domain !== null) {
			this.domain = domain;
		} else {
			this.domain = DEFAULT_DOMAIN;
		}
	}

	/**
	 *商品详情
	 *查询所有商品库存及详情
	 *	http://192.168.50.155:8080/admin/h/v2/backstage/getInventoryAll
	 */
	/*Magic.prototype.getProductDetails = function(success){
		$.get(this.domain + "/h/v2/backstage/getInventoryAll?time=" + new Date().getTime(), success, "json");
	 };*/
	Magic.prototype.getProductDetails = function(categoryId, success) {
		$.get(this.domain + "/h/v2/backstage/getInventoryAllByCategoryId?time=" + new Date().getTime(), {
			"categoryId": categoryId
		}, success, "json");
	};
	/** 
	 *支付模块
	 */

	/**
	 *alipay: 支付宝入口
	 */
	Magic.prototype.gotoAlipay = function(userId, orderId) {
		window.open(this.domain + "/doublepay/directpayapi.jsp?userId=" + userId + "&orderId=" + orderId, "_blank");
	}
	/**
	 *tenpay: 财付通
	 */
	Magic.prototype.gotoTenpay = function(userId, orderId) {
		// console.log(this.domain + "/tenpay/tenpayapi.jsp?userId=" + userId + "&orderId=" + orderId);
		window.open(this.domain + "/tenpay/tenpayapi.jsp?userId=" + userId + "&orderId=" + orderId, "_blank");
	}
	/**
	 *网银支付入口: 提供银行id
	 */
	Magic.prototype.gotoEbank = function(userId, orderId, bankId) {
		window.open(this.domain + "/ebank/ebankalipayapi.jsp?userId=" + userId + "&orderId=" + orderId + "&WIDdefaultbank=" + bankId, "_blank");
	}
	/**
	 * 一、购物车:
	 */
	/**
	 *根据数据库中的数据初始化
	 */
	Magic.prototype.initCartItemsWhenLogin = function(success) {
		$.post(this.domain + "/h/v2/cartItem/initCartItemsWhenLogin?time=" + new Date().getTime(), success, "json");
	};
	/**
	 *展示购物车中的商品
	 */
	Magic.prototype.showProsFromCart = function(success) {
		$.post(this.domain + "/h/v2/cartItem/getCartItems?time=" + new Date().getTime(), success, "json");
	};
	/**
	 *添加购物车
	 */
	Magic.prototype.addProIntoCart = function(productId, quantity, success) {
		// logger.log("productId:"+productId);
		$.post(this.domain + "/h/v2/cartItem/addCartItem?time=" + new Date().getTime(), {
			productId: productId,
			quantity: quantity
		}, success, "json");
	};
	/**
	 *更新购物车
	 */
	Magic.prototype.updateCart = function(cartjson, success) {
		$.post(this.domain + "/h/v2/cartItem/updateCart?time=" + new Date().getTime(), {
			cartjson: cartjson
		}, success, "json");
	};
	
	/**
	 *删除购物车商品
	 */
	Magic.prototype.delProFromCart = function(productId, success) {
		$.post(this.domain + "/h/v2/cartItem/removeCartItem?time=" + new Date().getTime(), {
			productId: productId
		}, success, "json");
	};


	/**
	 *二、优惠券:
	 */

	/**
	 *检查优惠券
	 */
	Magic.prototype.checkCoupon = function(couponNum, success) {
		$.post(this.domain + "/h/v2/coupon/checkCoupon?time=" + new Date().getTime(), {
			couponNum: couponNum
		}, success, "json");
	};
	/**
	 *查找当前用户的绑定优惠券
	 */
	Magic.prototype.getCoupons = function(success) {
		$.post(this.domain + "/h/v2/coupon/getCoupons?time=" + new Date().getTime(), success, "json");
	};
	/***三,地址模块
	1,获取已有地址
	http://192.168.150.85:8080/order/h/v2/address/getExistsAddress?userId=1
	2,
	保存地址
	http://192.168.150.85:8080/order/h/v2/address/saveAddress?
	userId=1&realname=测试汉字&province=测试汉字&city=测试汉字&country=测试汉字&street=1232sss&postalcode=10010&mobile=&telephone=154444
	*/

	/**
	 * 获取已有地址
	 */
	Magic.prototype.getExistsAddress = function(success) {
		$.post(this.domain + "/h/v2/address/getExistsAddress?time=" + new Date().getTime(), success, "json");
	};
	/**
	 * 获取默认地址
	 */
	Magic.prototype.getDefaultAddress = function(success) {
		$.post(this.domain + "/h/v2/address/getDefaultAddress?time=" + new Date().getTime(), success, "json");
	};

	/**
	 * 保存地址
	 * 参数:realname真实姓名,province省份,city城市,country县区,street街道,postalcode邮编,mobile电话,telephone手机
	 */
	Magic.prototype.saveAddress = function(obj, success) {
		$.post(this.domain + "/h/v2/address/saveAddress?time=" + new Date().getTime(), obj, success, "json");
	};

	/***四,订单模块:
		1,生成订单
		http://192.168.150.85:8080/order/h/v2/order/createOrder?
		参数:userId用户ID,couponnum优惠券号码,oisNeedReceipt是否需要发票(0否1是),oreceiptStyle发票类型(0个人,1公司),oreceiptCompany公司名称,realname真实姓名,province省份,city城市,country县区,street街道,postalcode邮编,mobile电话,telephone手机
		
		{"ret":1,"payentity":{"widseller_email":"乐跑手环","body":"乐跑手环","out_trade_no":"HL20131104082446185344","subject":"乐跑手环订购","widshow_url":"http://www.lepao.com","widreceive_name":"??Ժ??",
		"widreceive_zip":"10010","receive_phone":"154444","receive_mobile":"","widquantity":"11","widprice":2689.0,"logistics_type":"快递","logistics_fee":"0.00","logistics_payment":"卖家承担运费"},"msg":"success"}
		
		2,根据alipay返回结果,更新订单状态
		http://192.168.150.85:8080/order/h/v2/order/updateOrderStatus?oid=HL20131104062407477550&oalipayTradeNo=54321&ostatus=WAIT_SELLER_SEND
		参数:oid订单号,oalipayTradeNo支付宝交易号,ostatus订单状态
		{"ret":1,"UPDATE_RESULT":"UPDATED_BEFORE","msg":"success"}
		*/
	/**
	 * 生成订单
	 */

	Magic.prototype.createOrder = function(params, cartjson, success) {
		$.post(this.domain + "/h/v2/order/createOrder?time=" + new Date().getTime() + "&" + params, {
			cartjson: cartjson
		}, success, "json");
	};
	/*
	*腕带活动订单
	*/
	Magic.prototype.createBrandOrder = function(params, cartjson, success) {
		$.post(this.domain + "/h/v2/activity/createOrder?time=" + new Date().getTime() + "&" + params, {
			cartjson: cartjson
		}, success, "json");
	};
	/**
	 * 根据alipay返回结果,更新订单状态
	 */
	Magic.prototype.updateOrderStatus = function(oid, oalipayTradeNo, ostatus, success) {
		$.post(this.domain + "/h/v2/order/updateOrderStatus?time=" + new Date().getTime(), {
			oid: oid,
			oalipayTradeNo: oalipayTradeNo,
			ostatus: ostatus
		}, success, "json");
	};
	/**
	 * 订单管理-展示订单列表
	 */
	Magic.prototype.getOrderList = function(page, success) {
		$.post(this.domain + "/h/v2/order/getOrders?time=" + new Date().getTime(), {
			page: page
		}, success, "json");
	}
	/**
	 * 订单管理-物流信息
	 */
	Magic.prototype.getOrderStatus = function(orderId, success) {
		$.post(this.domain + "/h/v2/logistics/getOrderStatus?time=" + new Date().getTime(), {
			orderId: orderId
		}, success, "json");
	}
	/**
	 * 订单管理-物流状态 *new*
	 */
	Magic.prototype.getLogisticsInfo = function(orderId, success) {
		$.post(this.domain + "/h/v2/logistics/getLogisticsInfo?time=" + new Date().getTime(), {
			orderId: orderId
		}, success, "json");
	}
	/**
	 * 订单管理-订单详情
	 */
	Magic.prototype.getOrderContent = function(orderId, success) {
		$.post(this.domain + "/h/v2/order/getOrderDetails?time=" + new Date().getTime(), {
			orderId: orderId
		}, success, "json");
	}
	/**
	 *验证码
	 */
	Magic.prototype.userSubscribe = function(telphone, captchaCode, nickname, address, email, zipcode, lpcolor, producttype, success) {
		$.post(this.domain + "/h/v2/subscribe/userSubscribe?time=" + new Date().getTime(), {
			telphone: telphone,
			captchaCode: captchaCode,
			nickname: nickname,
			address: address,
			email: email,
			zipcode: zipcode,
			lpcolor: lpcolor,
			producttype: producttype
		}, success, "json");
	};
	Magic.prototype.sendTelCode = function(telphone, success) {
		$.post(this.domain + "/h/v2/user/sendTelCode?time=" + new Date().getTime(), {
			telphone: telphone
		}, success, "json");
	};
	Magic.prototype.login = function(account, password) {
		$.post(this.domain + "/h/v2/user/login?time=" + new Date().getTime(), {
			telphone: telphone
		}, success, "json");
	};
	/**
	 *用户信息
	 */
	Magic.prototype.getUserInfo = function(success) {
		$.post(this.domain + "/h/v2/user/myAccount?time=" + new Date().getTime(), success, "json");
	}

	Magic.prototype.weiboLogin = function(success) {
		// console.log()
		$.post(magicConstant.weiboAuthor.url, {
			"client_id": magicConstant.weiboAuthor.clientId,
			"redirect_uri": magicConstant.weiboAuthor.redirectUri,
			"scope": magicConstant.weiboAuthor.scope,
			"state": magicConstant.weiboAuthor.state
		}, success, "json");
	}
	//补全信息
	Magic.prototype.addUserInfo = function(params, success) {
		$.post(this.domain + "/h/v2/user/AutoComple?time=" + new Date().getTime(), {
			"platform": params.platform,
			"accessToken": params.accessToken,
			"captchaCode": params.captchaCode,
			"email": params.email,
			"password": params.password
		}, success, "json");
	}
	//修改邮箱
	Magic.prototype.changeEmail = function(email, captchaCode, success) {
		$.post(this.domain + "/h/v2/user/bindOrAlterEmail?time=" + new Date().getTime(), {
			"email": email,
			"captchaCode": captchaCode
		}, success, "json");
	}
	//重置密码
	Magic.prototype.restPassWord = function(token, passwd, success) {
		// http://localhost:8090/magic/page/resetPassword.html#confirm?token=emZjeWIyMDA5QDE2My5jb20=
		$.post(this.domain + "/h/v2/user/resetPasswd?time=" + new Date().getTime(), {
			token: token,
			passwd: passwd
		}, success, "json");
	};
	//设置密码
	Magic.prototype.changePassword = function(password, new_password, success) {
		// http://localhost:8090/magic/page/resetPassword.html#confirm?token=emZjeWIyMDA5QDE2My5jb20=
		$.post(this.domain + "/h/v2/user/changePassword?time=" + new Date().getTime(), {
			"password": password,
			"change_password": new_password
		}, success, "json");
	};
	//检测链接是否失效
	Magic.prototype.checkUrl = function(uuid, success) {
		$.post(this.domain + "/h/v2/user/isValidURL?time=" + new Date().getTime(), {
			uuid: uuid
		}, success, "json");
	};
	Magic.prototype.register = function(params, success) {
		if (regType == REG_TYPE_MAIL) {

			$.post(this.domain + "/h/v2/user/register?time=" + new Date().getTime(), {
				email_account: account,
				passwd: password,
				reg_type: regType
			}, success, "json");
		}
		if (regType == REG_TYPE_PHONE) {
			$.post(this.domain + "/h/v2/user/register?time=" + new Date().getTime(), {
				phone_account: account,
				passwd: password,
				reg_type: regType
			}, success, "json");
		}
	};

	//survey 
	Magic.prototype.survey = function(params, success) {
		$.post(this.domain + "/h/v2/user/lepaoSurvey?time=" + new Date().getTime(),params, success, "json");
	};

	//resendEmail 
	Magic.prototype.resendEmail = function(params, success) {
		$.post(this.domain + "/h/v2/user/resendActiveEmail?time=" + new Date().getTime(), {
			account: params
		}, success, "json");
	};

	/*
	1、获得用户官网的可以参加活动的订单数据
	192.168.50.192:8080/order/h/v2/activity/getOrderConfirmedData

	2、获取已验证腾讯爱逛订单信息
	192.168.50.192:8080/order/h/v2/activity/getTxConfirmedData

	3、获取已验证京东发票信息1、获得用户官网的可以参加活动的订单数据
	192.168.50.192:8080/order/h/v2/activity/getJDConfirmedData
	*/
	Magic.prototype.getOrderConfirmedData = function(target, success) {
		var apiFn = "";
		switch (target) {
			case "tengxun":
				apiFn = "getTxConfirmedData"
				break;
			case "jingdong":
				apiFn = "getJDConfirmedData"
				break;
			default:
				apiFn = "getOrderConfirmedData"
				break;
		}
		$.post(this.domain + "/h/v2/activity/" + apiFn + "?time=" + new Date().getTime(), success, "json");
	};
	Magic.prototype.confirmedData = function(target,params, success) {
		var apiFn = "";
		switch (target) {
			case "jingdong":
				apiFn = "confirmJDData"
				break;
			default:
				apiFn = "confirmTxData"
				break;
		}
		$.post(this.domain + "/h/v2/activity/" + apiFn + "?time=" + new Date().getTime(),params,success, "json");
	};
	Magic.prototype.countFreeBracelet = function(success) {
		$.post(this.domain + "/h/v2/activity/countFreeBracelet?time=" + new Date().getTime(),success, "json");
	};

	//获取编辑头像 token
	Magic.prototype.getToken = function(success) {
		$.get(this.domain + "/h/v2/user/getToken?time=" + new Date().getTime(),success, "json");
	};
	/*
	4、验证京东发票
	192.168.50.192:8080/order/h/v2/activity/confirmJDData?invoiceNo=121212&num=2

	5、验证腾讯爱逛信息
	192.168.50.192:8080/order/h/v2/activity/confirmTxData?txOrderId=121212&num=2&qq=23232

	6、获取用户可免费兑换的手环数量
	192.168.50.192:8080/order/h/v2/activity/confirmTxData/countFreeBracelet

	7、立即下单
	192.168.50.192:8080/order/h/v2/activity/createOrder?cartjson=和官网的相
	*/

	/*退换货*/

	// 创建退换货表单
	Magic.prototype.createReturnExchangeForm = function(orderId,success) {
		$.get(this.domain + "/h/v2/productReturn/createProductReturn?" + "orderId=" + orderId,success, "json");
	};

	// 重新申请退换货——创建退换货表单
	/*Magic.prototype.createReturnExchangeReapplicationForm = function(orderId,success) {
		$.get(this.domain + "/h/v2/productReturn/createProductReturnAgain?" + "orderId=" + orderId,success, "json");
	};*/

	//提交退换货商品信息
	Magic.prototype.submitReturnExchangeProduct = function(orderId,returnReason,buyChannel,serviceType,image,itemjson,returnId,success) {
		$.post(this.domain + "/h/v2/productReturn/createProductReturnPre",{
			orderId : orderId ,
			returnReason : returnReason ,
			buyChannel : buyChannel ,
			serviceType : serviceType ,
			image : image ,
			itemjson : itemjson,
			returnId : returnId 
		},success, "json");
	};

	//确认申请退换货表单信息
	Magic.prototype.confirmApplicationFormInfo = function(orderId,serviceType,success) {
		$.get(this.domain + "/h/v2/productReturn/createProductReturnConfirm?"+ "returnId=" + orderId + "&serviceType=" + serviceType,success, "json");
	};

	//确认提交申请退换货表单
	Magic.prototype.confirmSubmitApplicationForm = function(returnId,serviceType,pids,success) {
		$.post(this.domain + "/h/v2/productReturn/createProductReturnDone?"+ "returnId=" + returnId + "&serviceType=" + serviceType + "&pids=" + pids,success, "json");
	};

	//查看退换货审核状态
	Magic.prototype.getProductReturnStatus = function(returnId,serviceType,success) {
		$.get(this.domain + "/h/v2/productReturn/getProductReturnStatus?"+"returnId=" + returnId + "&serviceType=" + serviceType,success, "json");
	};

	//填写退换货物流信息
	Magic.prototype.fillLogisticsInfo = function(returnId,serviceType,success) {
		$.post(this.domain + "/h/v2/productReturn/getProductReturnInfoInput?"+"returnId=" + returnId + "&serviceType=" + serviceType,success, "json");
	};
	
	//提交退换货物流信息
	Magic.prototype.submitLogisticsInfo = function(returnId,expressId,expressNo,orderId,pids,refundAccount,success) {
		$.post(this.domain + "/h/v2/productReturn/createProductReturnInfo?"+ "returnId=" + returnId + "&expressId=" + expressId + "&expressNo=" + expressNo + "&orderId=" + orderId+ "&pids=" + pids + "&refundAccount=" + refundAccount,success, "json");
	};

	//查看换货物流信息
	Magic.prototype.getLogisticsTrackInfo = function(returnId,success) {
		$.post(this.domain + "/h/v2/productReturn/getLogisticsTrackInfo?"+ "returnId=" + returnId,success, "json");
	};

	//确认已收货或已退款
	Magic.prototype.createProductReturnComplete = function(returnId,orderId,success) {
		$.post(this.domain + "/h/v2/productReturn/createProductReturnComplete?"+ "returnId=" + returnId + "&orderId=" + orderId,success, "json");
	};

	//获取该用户下所有爱逛的订单信息
	Magic.prototype.getOrderListFromAiguangByUserId = function(success) {
		$.post(this.domain + "/h/v2/productReturn/getOrderListFromAiguangByUserId",success, "json");
	};

	//退换货-验证爱逛订单
	Magic.prototype.getOrderInfoByExternalId = function(orderId,buyerName,success) {
		$.post(this.domain + "/h/v2/productReturn/createProductReturnForAiguang?"+ "orderId=" + orderId + "&buyerName=" + buyerName,success, "json");
	};

});