
seajs.config({
	// Enable plugins
	plugins: ['shim', 'text'],

	// Configure alias
	alias: {
		'jquery': {
			src: 'http://dl.lepao.com/lib/jquery-1.8.1.min.js',
			exports: 'jQuery'
		}
		,
		
		'magicConfig':{
			src: 'MagicConfig.js',
			exports: 'magicConfig'
		},
		
		'jquery.upload': "lib/jquery.upload.js"
  },
  
  /** 防止缓存的配置 */
  map: [
        [ /^(.*\/js\/.*\.(?:css|js))(?:.*)$/i,           '$1?'],
        [ /^(.*\/js\/view\/.*\.(?:css|js))(?:.*)$/i,     '$1?'],
        [ /^(.*\/js\/page\/.*\.(?:css|js))(?:.*)$/i,     '$1?']
      ]
//  [ /^(.*\/js\/.*\.(?:css|js))(?:.*)$/i,           '$1?'+new Date().getTime() ],
//  [ /^(.*\/js\/view\/.*\.(?:css|js))(?:.*)$/i,     '$1?'+new Date().getTime() ],
//  [ /^(.*\/js\/page\/.*\.(?:css|js))(?:.*)$/i,     '$1?'+new Date().getTime() ]
});

