{{#each data}}
	{{#if isBrand}}
		<tr>
			<td data-pid="{{productId}}">
				<img src="{{image}}" width="70" height="70" />
			</td>
			<td>
				<span class="span01">{{name}}</span>
			</td>
			<td>
				<span class="item-price" data-color="{{color}}">{{favorablePrice}}</span>元（免费）
			</td>
			<td>
				<div class="span05 quantity-container clearfix">
				 	<span class="icon_Minus"></span>
				 	<input class="Quantity quantity-field" value="0" type="text" />
				 	<span class="icon_Plus"></span>
				 </div>
			</td>
		</tr>
	{{else}}
	<div class="cartBarClum list-item" >
		 <input class="check check-field"  type="checkbox" name="check-field" action-pid="{{productId}}" action-type="{{quantity}}"  checked="checked"  value=""/>
		 <div class="imgBox"><img src="{{image}}" width="70" height="70" /></div>
		 <span class="span01">{{name}}</span>
		 <span class="span02">{{color}}</span>
		 <span class="span03">￥<span class="item-price">{{favorablePrice}}</span></span>
		 <span class="span04">{{#if hasProduct}}{{hasProduct}}{{else}}<font color="red">该商品已经下架</font>{{/if}}</span>
		 <div class="span05 quantity-container">
		 	<span class="icon_Minus"></span>
		 	<input class="Quantity quantity-field" value="{{quantity}}" type="text" name="textfield" id="textfield" />
		 	<span class="icon_Plus"></span>
		 </div>
		 <a href="#" class="span06 delete">删除</a>
	</div>

	{{/if}}
{{/each}}
