define(function(require, exports, module){
	var $ = require('jquery');
	var ApplicationInfoTpl = require('./confirmApplicationInfo.html');
	var Handlebars = require('handlebars');
	var EventBus = require('../event/EventBus');
	var nodeId = "";
	module.exports = ApplicationInfo;
	
	function ApplicationInfo(nodeId){
		this.nodeId = nodeId;
		this.ApplicationInfoTemplate = Handlebars.compile(ApplicationInfoTpl);
	}

	/**
	 * 订单列表
	 * @param nodeId
	 * @param data
	 */
	
	ApplicationInfo.prototype.showApplicationInfo = function(data){
		var htmlStr = null;
		var me = this;
		// console.log(data)
		htmlStr = this.ApplicationInfoTemplate({
			data : data
		});

		$(this.nodeId).html(htmlStr); 
	};
	
	ApplicationInfo.prototype.bindPager = function(pager){
		$(this.nodeId).parent().parent().find(".pagination").html(pager.getHtml());
		pager.bindEvents();
	};
	
});