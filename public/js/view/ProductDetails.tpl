<!-- 商品详细信息模版 -->
 <div class="DetailsName">
          <span class="Dicon"></span>
          <span class="Dtxt"><b>{{name}}</b> {{description}}</span>
</div>
<div class="tab-panel" action-pid="{{productId}}">
  <div class="clearfix">
    <div class="DetailsTag">定<span class="span1">价：</span></div><div class="Price DeletLine">{{price}} 元</div>
  </div>
  <div class="clearfix">
    <div class="DetailsTag">优<span class="span2">惠</span><span class="span2">价：</span></div><div class="Price">{{favorablePrice}} 元</div>
  </div>
  <div class="clearfix">
    <div class="DetailsTag">促销信息：</div>
    <div class="Price promotion">
    {{#if promotions}}
        {{#each promotions}}
        {{#if links}}
         <a href="{{links}}" target="_blank" class="F_red">{{{description}}}</a><br>
         {{else}}
         <span class="F_red">{{{description}}}</span><br>
         {{/if}}
         {{/each}}
    {{else}}
      <p>暂无</p>
    {{/if}}
    </div>
  </div>
  
  <div class="clearfix">
    <div class="DetailsTag">库<span class="span1">存：</span></div><div class="Price"><span class="stock-num">{{qty}}</span> 在库</div>
  </div>
  <div class="clearfix">
    <div class="DetailsTag">预计发货：</div><div class="Price">{{time}}</div>
  </div> 
  <div class="clearfix">
    <div class="DetailsTag"></div><div class="Price">{{name}}&nbsp;已有{{buyNum}}人购买</div>
  </div>
</div>

<!-- <div class="tab-panel " action-pid="1">
  <div class="clearfix">
    <div class="DetailsTag">定<span class="span1">价：</span></div><div class="Price DeletLine">599元</div>
  </div>
  <div class="clearfix">
    <div class="DetailsTag">优<span class="span2">惠</span><span class="span2">价：</span></div><div class="Price">399元</div>
  </div>
  <div class="clearfix">
    <div class="DetailsTag">促销信息：</div><div class="Price"><p class="F_red">腾讯优惠码最高抵299元</p><p class="F_red">腾讯优惠码最高抵299元</p><p class="F_red">腾讯优惠码最高抵299元</p></div>
  </div>
  <p class="PriceTip">乐跑手环单色版（海洋蓝）&nbsp;已有83332人购买</p>
  <div class="clearfix">
    <div class="DetailsTag">库<span class="span1">存：</span></div><div class="Price"><span class="stock-num">53332</span> 在库</div>
  </div>
  <div class="clearfix">
    <div class="DetailsTag">预计发货：</div><div class="Price">2014年01月07日</div>
  </div>
</div> -->