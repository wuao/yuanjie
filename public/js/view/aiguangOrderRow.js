define(function(require, exports, module){
	var $ = require('jquery');
	var aiguangOrderInfoTpl = require('./aiguangOrderRow.html');
	var Handlebars = require('handlebars');
	var EventBus = require('../event/EventBus');
	var nodeId = "";
	module.exports = AiguangOrderInfo;
	
	function AiguangOrderInfo(nodeId){
		this.nodeId = nodeId;
		this.aiguangOrderInfoTemplate = Handlebars.compile(aiguangOrderInfoTpl);
	}

	/**
	 * 订单列表
	 * @param nodeId
	 * @param data
	 */
	
	AiguangOrderInfo.prototype.showAiguangOrderInfo = function(data){
		var htmlStr = null;
		var me = this;
		//console.log(data)
		htmlStr = this.aiguangOrderInfoTemplate({
			data : data
		});

		$(this.nodeId).html(htmlStr); 
	};
	
	AiguangOrderInfo.prototype.bindPager = function(pager){
		$(this.nodeId).parent().parent().find(".pagination").html(pager.getHtml());
		pager.bindEvents();
	};
	
});