define(function(require, exports, module) {
	var Logger = require('../util/Log');
	var logger = new Logger();
	var $ = require('jquery');
	var productDetailTpl = require('./ProductDetails.tpl');
	var tabButtonsTpl = require('./ProductDetails.TabButtons.html');
	var Handlebars = require('handlebars');
	var EventBus = require('../event/EventBus');


	module.exports = ProductDetails;

	function ProductDetails(data) {
		this.myData = data;
		this.productDetailsTemplate = Handlebars.compile(productDetailTpl);
		this.tabButtonsTemplate = Handlebars.compile(tabButtonsTpl);
	}
	/**
	 * 展示商品的内容
	 * @param nodeId
	 * @param data
	 */
	ProductDetails.prototype.showProductDetails = function(nodeId, productId) {
		// logger.log(productId);
		var htmlStr = null;
		var productData = this.findDataById(productId);
		// console.log(productData)
		htmlStr = this.productDetailsTemplate(productData);
		$(nodeId).html(htmlStr);
		// console.log((this.myData[productId-1]).image);
		$("#product-detail-img img")[0].src = (productData).image;
	};
	ProductDetails.prototype.findDataById = function(productId) {
		var me = this;
		if (me.myData === null) {
			logger.log("数据出错，应传入object 数据对象！");
			return "";
		}
		for (i in me.myData) {
			if (me.myData[i]["productId"] == productId) {
				return me.myData[i];
			}
		};
	};
	/**
	 * tab按钮展现
	 * @param nodeId
	 * @param data
	 */
	ProductDetails.prototype.showTabButtons = function(nodeId, callback) {
		// logger.log(this.myData);
		var htmlStr = null;
		htmlStr = this.tabButtonsTemplate({
			data: this.myData
		});
		$(nodeId).html(htmlStr);
		var $tabButton = $(nodeId).find("li");
		$tabButton.on("click", function() {
			if ($(this).hasClass("forbidden")) {
				return false;
			}
			$tabButton.removeClass('current');
			$(this).addClass('current');
			// $(this).attr("data-pid");
			callback($(this).attr("data-pid"));
		});
		// $(htmlStr).click(function(){
		// 	var event = jQuery.Event( "tab-icon",{id:id,nodeId:"sendFbFeed_"+id});
		// 	EventBus.trigger(event);
		// });
	};
});