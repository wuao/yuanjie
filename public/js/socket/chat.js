(function ($) {

    var ENTER = 13;
    var URL = 'http://' + window.location.host;
    var socket = io(URL);
    
    var $input = $('#inputText');
    var $sendBtn = $('#sendBtn');
    var $messages = $('#messages');

    $sendBtn.click(sendMessage);
    $input.keypress(enterClick);

    socket.on('chat', onChat);
    socket.on('ERROR', onError);

    function enterClick (e) {
        var key = e.which;
        if(key == ENTER)
        {
            sendMessage()
        }
    }

    function sendMessage () {
        var text = $input.val();
        socket.emit('chat', text);
        $input.val("");
    }

    function onChat (data) {
        data.time = moment(data.time * 1000).format('HH:mm:ss');
        var source = '<div class="topic-meta"><h2 class="topic_title"> \
                    <p class="love_chart_lovercolor" {{#unless self}} style="color:#F6762B" {{/unless}}>{{ user.nickname }}<span>{{ time }}</span></p> \
                </h2> \
                <p class="font_mysize magin_top" style="padding: 0px 0 5px 10px;"> \
                    {{ message }} \
                </p> \
            </div> \
            ';

        var template = Handlebars.compile(source);
        var html = template(data);
        $messages.append(html);
        $messages.scrollTop($messages[0].scrollHeight);
    }

    function onError (data) {
        if (data.error === "the pair user is not online") {
            alert('该用户不在线');
        }
    }

})(jQuery)