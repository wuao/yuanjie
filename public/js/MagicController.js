define(function(require, exports, module) {
	var $ = require('jquery'),
		Logger = require('./util/Log'),
		JSON = require('./util/JSON'),
		logger = new Logger(),
		magicConfig = require('./MagicConfig'),
		magicConstant = require('./MagicConstant'),
		WordPress = require('./cms/WordPress'),
		Magic = require('./cms/Magic'),
		PostList = require('./view/PostList.js'),
		// var Cookie = require('./util/Cookie.js'),
		DateParser = require("util/DateParser"),
		Md5 = require('./util/Md5.js'),
		ContentPage = require('./view/ContentPage.js'),
		ProductDetails = require('./view/ProductDetails.js'),
		EventBus = require('./event/EventBus'),
		CartRow = require('./view/CartRow.js'),
		OrderRow = require('./view/OrderRow.js'),
		ProductROW = require('./view/ProductROW.js'),
		ApplicationInfo = require('./view/confirmApplicationInfo.js'),
		VerifyStatusInfo = require('./view/verifyStatusInfo.js'),
		AiguangOrderRow = require('./view/aiguangOrderRow.js'),
		Payment = require('./view/Payment.js'),
		OrderDetails = require('./view/OrderDetails.js'),
		Dialog = require("./view/Dialog.js"),
		CheckInvoiceDialog = require("./view/CheckInvoiceDialog.js"),
		PagerView = require("./view/Pager/Pager.js"),
		ParseEmailUrl = require('./util/ParseEmailUrl.js');
	module.exports = MagicController;
	/*var console = console || {
		log: function() {
			return false;
		}
	}*/
	function MagicController() {
		this.config = magicConfig; 
		this.cms = new WordPress(magicConfig.cmsBase);
		this.magicService = new Magic(magicConfig.magicBase);
		this.cartRow = new CartRow();
		this.dialog = new Dialog();
		this.checkInvoiceDialog = new CheckInvoiceDialog();
		this.Payment = new Payment();
	};


	/*Utils*/
	MagicController.prototype.sendEmailRepo = function(email, repoType) {
		ParseEmailUrl.getUrl(email);
		this.showAlertDialog({
			title: "温馨提示",
//			content: "已发送验证邮件至您" + repoType + "的邮箱{" + email + "}，请前往邮箱点击邮件内链接完成" + repoType + "。",
			content: "恭喜您，您已在乐跑官网注册成功，请立即登录。"
//			positiveText: "去激活"
		}, function() {
			window.open("http://" + ParseEmailUrl.getUrl(email), "_blank")
		});
	};

	/***购物车模块 */


	/**
	 * 加入购物车
	 * @param productId商品ID,quantity商品数量
	 */
	MagicController.prototype.addProIntoCart = function(productId, quantity, handler) {
		var me = this;
		me.magicService.addProIntoCart(productId, quantity, handler);
	};

	/**
	 * 更新购物车
	 */
	MagicController.prototype.updateCart = function(cartjson) {
		var me = this;

		var cartjsonStr = JSON.stringify(cartjson);
		me.magicService.updateCart(cartjsonStr, function(data) {
			var cartitems = new Array();
			for (var i = 0; i < cartjson.length; i++) {
				cartitems.push(cartjson[i]);
			};
			if (data.ret == 1) {
				var cartitemsStr = JSON.stringify(cartitems);
				location.href = magicConstant.targetUrl.userBuyInfo + "?cart_json=" + encodeURI(cartitemsStr);
			} else {
				me.showAlertDialog(data.msg);
			};
		});
	};
	/**
	 * 更新腕带购物车
	 */
	MagicController.prototype.updateBrandCart = function(cartjson) {
		var me = this;
		var cartjsonStr = JSON.stringify(cartjson);
		// alert(cartjsonStr);
		location.href = magicConstant.targetUrl.brandShoppingInfo + "?isBrand=1&" + "?cart_json=" + encodeURI(cartjsonStr);
	};
	/**前往购物车*/
	MagicController.prototype.goToCart = function() {
		location.href = magicConstant.targetUrl.cart;
	}
	/**检查购物车商品 是否为空*/
	MagicController.prototype.checkCart = function(success) {
		var me = this;
		me.magicService.showProsFromCart(success);
	};
	/**
	 * 展示购物车商品
	 *	@CONTEX "cartitems":{"userId":"1","productId":1,"cisDel":0,"quantity":5}
	 */
	MagicController.prototype.showCartRow = function(nodeId, totalPriceNode, totalPriceIncludePost, totalQuantityNode, callback) {
		var me = this;
		var userId = 1;
		//	 	Cookie.setCookie("test_cookie","damon",7);
		me.magicService.showProsFromCart(function(data) {
			// console.log("展示购物车商品");
			// logger.log(data);
			if (data.ret == 1) {
				var cartList = new Array();
				try {
					var cartitems = data.cartitems;
					// logger.log(data.cartitems);
					// var cartitems = [{"userId":"1","productId":1,"cisDel":0,"quantity":5},{"userId":"1","productId":3,"cisDel":1,"quantity":1},{"userId":"1","productId":4,"cisDel":0,"quantity":5}];
					var totalPrice = 0;
					var totalQuantity = 0;
					if (cartitems.length > 0) {
						var product = null;
						for (var i = 0; i < cartitems.length; i++) {
							if (cartitems[i].isWithdraw == 2) {
								cartitems[i]["hasProduct"] = "有货";
							}
						};
						me.cartRow.showCartRow(nodeId, cartitems);
						$(totalPriceNode).html(totalPrice);
						$(totalPriceIncludePost).html(totalPrice);
						$(totalQuantityNode).html(totalQuantity);
						callback();
					} else {
						$(nodeId).parent(".shopping-list").html("没有商品额。<a color='blue' href='" + magicConstant.targetUrl.productDetails + "'>去购物</a>").css("color", "gray");
					}

				} catch (e) {
					logger.log(e);
				}

			} else {
				$(nodeId).html(data.msg).css("color", "red");
			}
		});

	};
	/**
	 * 删除购物车
	 */
	MagicController.prototype.delProFromCart = function(productId, success) {
		var me = this;
		me.magicService.delProFromCart(productId, success);
	};
	/***优惠券模块*/
	/**
	 *检查优惠券
	 * error:不存在1009，  已经使用1010， 已经过期1011
	 */
	MagicController.prototype.checkCoupon = function(couponNum, success) {
		var me = this;
		me.magicService.checkCoupon(couponNum, success);
	};
	/**
	 *查找当前用户的绑定优惠券
	 */
	MagicController.prototype.getCurrentCoupons = function(nodeId) {
		var me = this;
		me.magicService.getCoupons(function(data) {
			if (data.ret == 1) {
				// console.log(data);
				var coupons = data.coupons;
				if (coupons.length != 0) {
					$(".coupon-empty").remove();
					for (var i = 0; i < coupons.length; i++) {
						$(nodeId).append("<div class='coupon'>" + coupons[i].couponNum + "</div>");
					};
				}
			} else {
				$(nodeId).html("<div class='coupon-empty'>" + data.msg + "</div>");
			}
			// console.log("绑定优惠券" + data.msg);
		});
	};


	/***订单模块*/

	/**生成订单
	 *userId,couponnum,oisNeedReceipt,oreceiptStyle,oreceiptCompany,realname,province,city,country,street,postalcode,mobile,telephone
	 */
	MagicController.prototype.createOrder = function(params, cartjson) {
		var isClickAble = true;
		var me = this;
		var PAY_ONLINE = 1,
			PAY_OFFLINE = 0,
			NEED_PAY = 1;
		if (!isClickAble) {
			return false;
		}
		me.magicService.createOrder(params, cartjson, function(data) {
			// console.log(data);
			if (data.ret == 1) {
				if (data.payType === PAY_ONLINE && data.payCode === NEED_PAY) {
					location.href = magicConstant.targetUrl.paymentType + "?orderId=" + data.orderId;
				} else {
					location.href = magicConstant.targetUrl.myOrder;
				}
				isClickAble = false;
			} else {
				isClickAble = true;
				me.showAlertDialog(data.msg);
			}
		});

	};
	/**生成腕带订单
	 */
	MagicController.prototype.createBrandOrder = function(params, cartjson) {
		var isClickAble = true;
		var me = this;
		if (!isClickAble) {
			return false;
		}
		me.magicService.createBrandOrder(params, cartjson, function(data) {
			// console.log(data);
			if (data.ret == 1) {
				location.href = magicConstant.targetUrl.paymentType + "?orderId=" + data.orderId;
				isClickAble = false;
			} else {
				isClickAble = true;
				me.showConfirmDialog({
					content: data.msg,
					negtiveText: "知道了",
					positiveText: "返回首页"
				}, function(e) {
					location.href = magicConstant.targetUrl.indexPage;
				});
			}

		});

	};

	/**
	 *展示订单商品
	 */
	//var cartJson = urlParams.getUrlParams("cart_json");
	MagicController.prototype.showUserBuyInfo = function(nodeId, totalPriceNode, amountNode, cartJsonStr, isBrand) {
		var me = this;
		var totalPrice = 0;
		var totalNum = 0;
		var userbuyinfo;
		var productStr = "";
		var postage = 15;
		try {
			var cartJson = JSON.parse(decodeURI(cartJsonStr));
		} catch (e) {
			$(nodeId).html("没有下单的商品。");
			return false;
		}
		if (cartJson.length > 0) {
			if (isBrand) {
				for (i in cartJson) {
					userbuyinfo = cartJson[i];
					// logger.log()
					totalNum += parseInt(cartJson[i].quantity);
					if (cartJson[i].quantity > 0) {
						productStr += '<tr><td>' + userbuyinfo.name + '</td><td>' + cartJson[i].quantity + '</td></tr>'
					};
				}
				totalPrice = postage; //￥15邮费
				$(".postage").html(postage);
			} else {
				for (i in cartJson) {
					userbuyinfo = cartJson[i];
					// logger.log()
					totalNum += parseInt(cartJson[i].quantity);
					totalPrice += cartJson[i].quantity * cartJson[i].price;
					productStr += '<tr><td>' + userbuyinfo.name + '</td><td>' + userbuyinfo.price + '</td><td>' + cartJson[i].quantity + '</td><td>' + cartJson[i].quantity * userbuyinfo.price + '</td></tr>'
				}
			}
			$(nodeId).html(productStr);
			$(totalPriceNode).html(totalPrice);
			$(amountNode).html(totalNum);
		}

	};

	/**
	 *支付模块
	 */

	/*支付页面订单详情*/
	MagicController.prototype.gotoPay = function(userId, orderId, bankId) {
		var me = this;

		if (bankId != "" && bankId != null && bankId != undefined) {
			switch (bankId) {
				case "TENPAY":
					me.magicService.gotoTenpay(userId, orderId);
					break;
				case "ALIPAY":
					me.magicService.gotoAlipay(userId, orderId);
					break;
				default:
					me.magicService.gotoEbank(userId, orderId, bankId);
					break;
			}
			me.showConfirmDialog({
				content: "支付是否成功？",
				negtiveText: "未完成",
				positiveText: "支付成功"
			}, function(e) {
				location.href = magicConstant.targetUrl.myOrder;
			});
			/*me.showConfirmDialog("支付是否成功","未完成","支付成功"){
			}*/
		} else {
			me.showAlertDialog("请选择支付方式！");
		}
	};
	MagicController.prototype.showPaymentOrderContent = function(nodeId, orderId, callback) {
		var me = this;
		me.magicService.getOrderContent(orderId, function(data) {
			if (data.ret == 1) {
				var myOrderItems = data.orderItems;
				/*var totalPrice = 0;
				for (i in myOrderItems) {
					totalPrice += myOrderItems[i].price * myOrderItems[i].num;
				}
				data["totalPrice"] = totalPrice;*/
				
				me.Payment.showOrderContent(nodeId, data);
				callback(data);
			} else {
				$(nodeId).html(data.msg).css("color", "red");
			}
		});
	}


	/**
	 * 根据alipay返回结果,更新订单状态
	 */
	MagicController.prototype.updateOrderStatus = function(oid, oalipayTradeNo, ostatus) {
		var me = this;
		me.magicService.createOrder(oid, oalipayTradeNo, ostatus, function(data) {
			//TODO 更新订单状态

		});
	};

	/***地址模块*/
	/**
	 * 获取已有地址
	 */
	MagicController.prototype.getExistsAddress = function(callback) {
		var me = this;
		me.magicService.getExistsAddress(callback);
	};
	/**
	 * 获取默认地址
	 */
	MagicController.prototype.getDefaultAddress = function(callback) {
		var me = this;
		me.magicService.getDefaultAddress(callback);
	};

	/**
	 * 保存地址
	 * 参数:userId用户ID,realname真实姓名,province省份,city城市,country县区,street街道,postalcode邮编,mobile电话,telephone手机
	 */
	MagicController.prototype.saveAddress = function(obj, callback) {
		var me = this;
		me.magicService.saveAddress(obj, function(data) {

			if (data.ret == 1) {
				callback(data);
			} else {
				me.showAlertDialog(data.msg);
			}

		});
	};

	/**
	 * 展示商品数据
	 */
	MagicController.prototype.showProductDetails = function(nodeId, tabId, categoryId) {
		var me = this;
		var isSellup = true;
		var defaultProId = 1;
		var myProList = new Array();
		me.magicService.getProductDetails(categoryId, function(data) {
			if (data.ret === 1) {
				var myPl = data.inventoryList;
				var myTime = new Date().getTime()
				var dateParser = new DateParser();
				// var isSetDefault = false;
				for (var i = 0; i < myPl.length; i++) {
					defaultProId = myPl[0].productId;
					if (i == 0) {
						myPl[i]["current"] = "current";
					}
					if (myPl[i].isWithdraw === 1) {
						myPl[i]["forbidden"] = "forbidden";
					} else {
						/*if(!isSetDefault){
							defaultProId = ++i;
							isSetDefault = true;
						}*/
						isSellup = false;
					}

					if (myTime >= myPl[i].deliveryTime) {
						myPl[i]["time"] = "现货"
					} else {
						myPl[i]["time"] = dateParser.parseToYMDCN(myPl[i].deliveryTime);
					}
				};
				if (isSellup) {
					location.href = magicConstant.targetUrl.preOrder + "?isSellup=true";
				}
				ProductDetails = new ProductDetails(myPl);
				ProductDetails.showProductDetails(nodeId, defaultProId);
				ProductDetails.showTabButtons(tabId, function(proId) {
					ProductDetails.showProductDetails(nodeId, proId);
				});
			} else {
				$(nodeId).html();
			}
		});
	};

	/**
	 * Dialog
	 */
	MagicController.prototype.showConfirmDialog = function(params, callback) {
		var me = this;
		me.dialog.show(params, true, function(e, nodeId, $maskLayer) {
			$(nodeId).remove();
			$maskLayer.fadeOut(300, function() {
				$(this).remove();
				if (callback) {
					callback();
				};
			});
		});
	};
	MagicController.prototype.showAlertDialog = function(params, callback) {
		var me = this;
		me.dialog.show(params, false, function(e, nodeId, $maskLayer) {
			$(nodeId).remove();
			$maskLayer.fadeOut(300, function() {
				(callback && typeof(callback) === "function") && callback();
				$(this).remove();
			});
		});
	};
	
	MagicController.prototype.showAlertFailDialog = function(aa,callback){
		var me = this;
		me.dialog.show(aa,false,function(e,nodeId,$maskLayer){
			$(nodeId).remove();
			$maskLayer.fadeOut(300,function(){
				(callback && typeof(callback) === "function") && callback();
				$(this).remove();
			});
		});
	};
	/*免费送腕带活动*/
	/*CheckInvoiceDialog*/
	MagicController.prototype.showCheckInvoicePanel = function() {
		var me = this,
			bandAmount = 0,
			target = "",
			records = [];
		me.checkInvoiceDialog.show();
		me.magicService.countFreeBracelet(function(data) {
			if (data.ret === 1) {
				bandAmount = data.count;
			} else {
				bandAmount = data.msg;
			}
			me.checkInvoiceDialog.showAmount(bandAmount);
		});
		// magicService.getconfirm
		me.magicService.getOrderConfirmedData("lepao", function(data) {
			if (data.ret === 1) {
				if (data.data.length <= 0) {
					me.checkInvoiceDialog.renderResultList(null);
				} else {
					records = me.invoiceDataModify(data.data);
					me.checkInvoiceDialog.renderResultList(records);
				}
			}
		});

		EventBus.bind("change.tabs", function(data) {
			records = [];
			target = data.target;
			$(data.context).find("button").removeClass("active");
			$(data.context).find(">[data-type='" + target + "']").addClass("active");
			me.checkInvoiceDialog.renderForm(target);
			me.magicService.getOrderConfirmedData(target, function(data) {
				if (data.ret === 1) {
					if (data.data.length <= 0) {
						me.checkInvoiceDialog.renderResultList(null);
					} else {
						records = me.invoiceDataModify(data.data);
						// console.log(records);
						me.checkInvoiceDialog.renderResultList(records);
					}
				}
			});
			// me.checkInvoiceDialog.renderResultList(records);
		});
		EventBus.bind("submit.check_invoice", function(carrier) {
			me.magicService.confirmedData(target, carrier.params, function(data) {
				if (data.ret === 1) {
					records.unshift(carrier.record);
					// $.extend(records, data.record);
					if (!isNaN(bandAmount)) {
						bandAmount += parseInt(carrier.record.num);
						me.checkInvoiceDialog.showAmount(bandAmount);
					}
					me.checkInvoiceDialog.renderResultList(records);
				} else {
					me.checkInvoiceDialog.showError($(carrier.context), data.msg);
				}
			});
		});
		$(".positive_btn").on("click", function(event) {
			event.preventDefault();
			if (bandAmount > 0) {
				location.href = magicConstant.targetUrl.brandCart + "?effectiveNum=" + bandAmount;
			} else {
				me.showAlertDialog("抱歉！您没有腕带可以领取！");
			}
		});
	};
	MagicController.prototype.invoiceDataModify = function(data) {
		var arr = [];
		// console.log(data)
		for (var i = data.length - 1; i >= 0; i--) {
			var record = data[i];
			// console.log(record)
			if (record.isUsed === 1) {
				record["useable"] = "useable";
			}
			arr.push(record);
		}
		return arr.reverse();
	};

	/*brand-cart*/
	/**
	 * 展示送腕带商品
	 *	@CONTEX "cartitems":{"userId":"1","productId":1,"cisDel":0,"quantity":5}
	 */
	MagicController.prototype.showBrandCartRow = function(nodeId, totalPriceNode, totalPriceIncludePost, totalQuantityNode, callback) {
		var me = this,
			categoryId = 4;
		me.magicService.getProductDetails(categoryId, function(data) {
			if (data.ret == 1) {
				var cartList = new Array();
				try {
					var cartitems = data.inventoryList,
						totalPrice = 0,
						totalQuantity = 0;
					if (cartitems.length > 0) {
						var product = null;
						for (var i = 0; i < cartitems.length; i++) {
							if (cartitems[i].isWithdraw == 2) {
								cartitems[i]["hasProduct"] = "有货";
							}
							cartitems[i]["isBrand"] = "isBrand";
						};
						me.cartRow.showCartRow(nodeId, cartitems);
						$(totalPriceNode).html(totalPrice);
						$(totalPriceIncludePost).html(totalPrice);
						$(totalQuantityNode).html(totalQuantity);
						callback();
					} else {
						$(nodeId).parent(".shopping-list").html("没有商品额。<a color='blue' href='" + magicConstant.targetUrl.productDetails + "'>去购物</a>").css("color", "gray");
					}

				} catch (e) {
					logger.log(e);
				}

			} else {
				$(nodeId).html(data.msg).css("color", "red");
			}
		});

	};

	/**header模块*/
	/*MagicController.prototype.sendTelCode = function(telephone) {
		var me = this;
		me.magicService.sendTelCode(telphone, function(data) {
			$('.submit_code').val('5分钟内有效').attr("disabled", "true");
			$('.input-text').attr("disabled", "true");
			alert("验证码已经发送，请查收");
			if (data.ret == 1) {

			} else {
				alert(data.msg);
			}
		});
	};
	MagicController.prototype.login = function() {
		me.closeWin();
		me.resetForm();
	};
	MagicController.prototype.register = function(account, password, regType) {
		var me = this;
		console.log("register start!");
		me.magicService.register(account, password, regType, function(data) {
			console.log(data);
			if (data.ret == 1) {
				me.closeWin();
				alert("注册成功");
				me.resetForm();
			} else {
				alert("注册出错：" + data.msg);
			}
		});
	};*/

	MagicController.prototype.showNewsList = function(category, nodeId) {
		this.cms.getCategoryPosts({
			slug: category,
			include: "title,date"
		}, function(data) {
			var postList = new PostList();
			if (data.status === "ok") {
				for (m in data.posts) {
					data.posts[m].date = data.posts[m].date.substring(5, 10);
				}
			}
			postList.showPostList(nodeId, data);
		});
	};


	MagicController.prototype.showNewsPageListPage = function(nodeId, page, category) {
		var me = this;
		me.cms.getCategoryPosts({
			slug: category,
			page: page,
			count: 12,
			include: "title,date,categories"
		}, function(data) {
			// console.log(data)
			var postList = new PostList();
			postList.showNewsPageList(nodeId, data, page);
			postList.bindPageEvents();
			var totalPages = data.pages;
			EventBus.bind("pre", function(e) {
				var currentPage = e.currentPage;
				if (currentPage <= 1) {
					alert("不能再往前了！");
					return false;
				}
				me.showNewsPageListPage(nodeId, parseInt(currentPage) - 1, category);
			});
			EventBus.bind("next", function(e) {
				var currentPage = e.currentPage;
				if (currentPage >= totalPages) {
					alert("已经到尾页了！");
					return false;
				}
				me.showNewsPageListPage(nodeId, parseInt(currentPage) + 1, category);
			});
			EventBus.bind("first", function(e) {
				me.showNewsPageListPage(nodeId, 1, category);
			});
			EventBus.bind("end", function(e) {
				me.showNewsPageListPage(nodeId, totalPages, category);
			});
		});
	};

	MagicController.prototype.showContentPage = function(nodeId, id) {
		this.cms.getPost(id, function(data) {
			var contentPage = new ContentPage();
			contentPage.showContentPage(nodeId, data);
		});
	};

	//FIXME ?这块可以干掉了吧
	MagicController.prototype.activationCode = function() {
		var me = this;
		var domain = 'http://shenqu.hoolai.com',
			cdkeyIput = $('.input-text'),
			validInput = $('.input-key'),
			realname = $('.realname'),
			street = $('.street'),
			subButton = $('.submit_code'),
			submit = $('.submit_reserve'),
			postcode = $('.postcode'),
			email = $('.email');

		subButton.on('click', function() {
			var keyVal = cdkeyIput.val();
			subKey(keyVal);
		});

		function subKey(keyVal) {
			if (isLegalkey(keyVal)) {
				var telphone = $('.input-text').val();
				me.magicService.sendTelCode(telphone, function(data) {
					$('.submit_code').val('5分钟内有效').attr("disabled", "true");
					$('.input-text').attr("disabled", "true");
					alert("验证码已经发送，请查收");
					if (data.ret == 1) {

					} else {
						alert('系统出错，请稍后再试');
					}
				});
			}
		}

		function isLegalkey(keyNum) {
			if (keyNum.trim() == '' || keyNum.trim() == '请填写手机号码') {
				alert('您还没有填写手机号！');
				cdkeyIput.attr("value", "").focus();
				return false;
			} else if (!(/^1[3,4,5,8]{1}[0-9]{9}$/.test(keyNum))) {
				alert('手机号错误，请确保您的手机号为11位数字且正确书写∩_∩~~');
				cdkeyIput.attr("value", "").focus();
				return false;
			} else return true;
		}

	}


	MagicController.prototype.Reservation = function() {
		var me = this;
		$('.goods_color a').on('click', function() {
			$('.selected').removeClass('selected');
			$(this).addClass('selected');
		});
		$('.submit_reserve').on('click', function() {
			//FIXME　暂时改为验证码
			var captchaCode = $("input[name=captchaCode]").val();
			//			var telCode = $('.input-key').val();
			var telphone = $('.input-text').val();
			var nickname = $('.realname').val();
			var shi = $('#Select1').val();
			var xian = $('#Select2').val();
			var site = $('#Select3').val();
			var address = shi + xian + site;
			var email = $('.email').val();
			var zipcode = $('.postcode').val();
			var lpcolor = $('.goods_color a.selected').text();
			var producttype = $('.producttype').text();
			var emailExp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

			if (captchaCode == null || captchaCode == '' || captchaCode == undefined || captchaCode == "请填写验证码") {
				alert("请填写验证码！");
				return;
			}
			if (!emailExp.test(email)) {
				alert("邮箱格式不正确");
				return false;
			}
			me.magicService.userSubscribe(telphone, captchaCode, nickname, address, email, zipcode, lpcolor, producttype, function(data) {
				if (data.ret == 1) {
					document.location.href = magicConstant.targetUrl.success;
				} else {
					alert(data.msg);
				}
			});
		})

	};

	/**刷新验证码*/
	MagicController.prototype.refreshVlidCode = function(nodeId) {
		var date = new Date();
		var domain = magicConfig.magicBase;
		$(nodeId).attr('src', domain + '/image/identifying?' + date.getSeconds());
	}
	/**重置密码*/
	MagicController.prototype.restPassWord = function(nodeId, token, psw) {
		var me = this;
		me.magicService.restPassWord(token, Md5.hex_md5(psw), function(data) {

			if (data.ret == 1) {
				$(nodeId).html("重置密码成功，等待<a href='" + magicConstant.targetUrl.indexPage + "'>跳转</a>...");
				var timeOut = setTimeout(function() {
					location.href = magicConstant.targetUrl.indexPage;
				}, 3000);

			} else {
				$(nodeId).html(data.msg);
			}
		});
	}

	/**用户信息**/
	MagicController.prototype.getUserInfo = function(callback) {
		var me = this;
		me.magicService.getUserInfo(callback);
	}
	/**微博 第三方登录 **/
	MagicController.prototype.weiboLogin = function() {
		var me = this;
		me.magicService.weiboLogin(function(data) {
			// console.log(data);

		});
	};
	/**补全信息**/
	MagicController.prototype.addUserInfo = function(params, node) {
		var me = this;
		params.password = Md5.hex_md5(params.password);
		me.magicService.addUserInfo(params, function(data) {
			if (data.ret == 1) {
				// Cookie.setCookie("account",params.email);
				// Cookie.setCookie("_pd",params.password);
				node.html("绑定邮箱成功，您可以使用绑定的邮箱账号登陆。");
//				node.html("已发送邮件至您的邮箱{" + params.email + "}，请登录该邮箱点击邮件内连接，完成信息补全。<a href='http://" + ParseEmailUrl.getUrl(params.email) + "' target='_blank'>前往您的邮箱</a>。");
			} else {
				me.showAlertDialog(data.msg);
			}
		});
	}

	/**修改邮箱**/
	MagicController.prototype.changeEmail = function(email, captchaCode) {
		var me = this;
		me.magicService.changeEmail(email, captchaCode, function(data) {
			if (data.ret == 1) {
				//修改成功
				me.sendEmailRepo(email, "修改");
			} else {
				me.showAlertDialog(data.msg);
			}
		});
	}

	/**修改密码**/
	MagicController.prototype.changePassword = function(password, new_password) {
		var me = this;

		me.magicService.changePassword(Md5.hex_md5(password), Md5.hex_md5(new_password), function(data) {
			if (data.ret == 1) {
				//修改成功
				me.showAlertDialog("您的密码已成功修改！", function() {
					location.href = magicConstant.targetUrl.personalPage;
				});
			} else {
				me.showAlertDialog(data.msg);
			}
		});
	}

	/**校验找回密码链接**/
	MagicController.prototype.checkUrl = function(nodeId, uuid, callback) {
		var me = this;
		me.magicService.checkUrl(uuid, function(data) {
			if (data.ret == 1) {
				$(nodeId).load(magicConstant.targetUrl.RestPsw, function() {
					if (callback && typeof(callback) === "function") {
						callback();
					}
				});
			} else {
				$(nodeId).html(data.msg);
			}
		});
	}

	/** 展示订单列表*/
	MagicController.prototype.showOrderList = function(nodeId, page) {
		var me = this;
		var orderRow = new OrderRow(nodeId);
		var STATUS_WAIT = "等待付款",
			STATUS_FAIL = "付款失败",
			STATUS_SUCCESS = "付款成功";
		var ACTION_PAY = "付款",
			ACTION_CLOSE = "关闭",
			ACTION_CHECk = "查看";
		me.magicService.getOrderList(page, function(data) {
			if (data.ret == 1) {
				var orderList = data.orderList;
				var orderRepoList = new Array();
				var dateParser = new DateParser();
				if (orderList.length <= 0) {
					$(nodeId).html("没有下单的商品。");
					return false;
				}
				for (i in orderList) {
					var productRepo = orderList[i].products[0];
					var statusName = "";
					var statusAction = "";
					var statusActionUrl = "javascript:void(0)";
					if(parseInt(orderList[i].type)==1){//只展示官网订单标识
						orderList[i].channelType = 1;
					}
					//orderList[i].status = 2;
					switch (orderList[i].status) {

						case 4:
							statusName = ACTION_CLOSE
							orderList[i]["checkDetail"] = ACTION_CHECk
							orderList[i]["checkUrl"] = magicConstant.targetUrl.orderDetail + "?orderId=" + orderList[i].orderNum
							break;

						case 3:
							statusActionUrl = magicConstant.targetUrl.paymentType + "?orderId=" + orderList[i].orderNum
							statusName = STATUS_WAIT
							statusAction = ACTION_PAY
							orderList[i]["checkDetail"] = ACTION_CHECk
							orderList[i]["checkUrl"] = magicConstant.targetUrl.orderDetail + "?orderId=" + orderList[i].orderNum
							break;
						case 2:
							statusName = STATUS_SUCCESS
							statusActionUrl = magicConstant.targetUrl.orderDetail + "?orderId=" + orderList[i].orderNum
							statusAction = ACTION_CHECk
							productReturnExchangeEntry(i);
							break;
						default:
							statusActionUrl = magicConstant.targetUrl.paymentType + "?orderId=" + orderList[i].orderNum
							statusName = STATUS_WAIT
							statusAction = ACTION_PAY
							orderList[i]["checkDetail"] = ACTION_CHECk
							orderList[i]["checkUrl"] = magicConstant.targetUrl.orderDetail + "?orderId=" + orderList[i].orderNum
							break;

					}
					orderList[i]["statusName"] = statusName;
					orderList[i]["statusAction"] = statusAction;
					orderList[i]["statusActionUrl"] = statusActionUrl;
					orderList[i]["createTimeStr"] = dateParser.parseToYMD(orderList[i].createTime);
					orderList[i]["imageUrl"] = productRepo.image;
					orderList[i]["productName"] = productRepo.name;
					if (orderList[i].productNum > 1) {
						orderList[i]["quantity"] = orderList[i].productNum;
					}

					
				};

				function productReturnExchangeEntry(index){
					var i = index;
					//orderList[i].payTime = new Date().getTime();
					//判断退换货过期时间
					var time = orderList[i].payTime + 1728000000;//默认付款后5天收货+15天=20天
					var today = new Date().getTime();
					if(today>time){//过期
						orderList[i].expire = 1;
					}

					//退换货权利判断
					if(!orderList[i].expire){
						orderList[i].productReturnExchangeRight = 1;
					}

					//被退换商品id
					var products = orderList[i].products;
					var pidArray = [];
					for(k in products){
						if(products[k].isReturn){
							pidArray.push(products[k].id);
						}	
					}
					orderList[i].pids = pidArray.join(',');
					
					for(j in products ){
						var servieType = "";
						var verifyStatus = "";
						var toStatusUrl = "javascript:void(0)";
						switch (products[j].isReturn) {
							case 0:
								products[j]["servieType"] = "退货/换货";
								products[j]["toStatusUrl"] = "return-exchange-application.html" + "?orderId=" + orderList[i].orderNum + "&userId=" + orderList[i].userId + "&type=" + orderList[i].type;
								break;
							case 1:
								products[j]["servieType"] = "退货";
								switch (products[j].isCheck) {
								
									case 1:
										products[j]["verifyStatus"] = "审核中";
										products[j]["toStatusUrl"] = "./service-verify-verifying.html" + "?orderId=" + orderList[i].orderNum +"&serviceType=1"+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
										break;
									case 2://审核通过
										products[j]["verifyStatus"] = "审核通过";
										products[j]["toStatusUrl"] = "./service-verify-passed.html" + "?orderId=" + orderList[i].orderNum +"&serviceType=1"+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
										break;
									case 3:
										products[j]["verifyStatus"] = "退货中";
										products[j]["toStatusUrl"] = "./productReturn.html" + "?orderId=" + orderList[i].orderNum+"&serviceType=1"+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
										break;
									case 4:
										products[j]["verifyStatus"] = "确认已退款";
										products[j]["toStatusUrl"] = "./return-exchange-finish.html" + "?orderId=" + orderList[i].orderNum +"&serviceType=1"+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
										break;
									case 5:
										products[j]["verifyStatus"] = "拒绝退货";
										products[j]["toStatusUrl"] = "./service-verify-refusal.html" + "?orderId=" + orderList[i].orderNum +"&serviceType=1" + "&userId=" + orderList[i].userId+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
										break;
									case 6:
										products[j]["verifyStatus"] = "退货完成";
										products[j]["toStatusUrl"] = "./CustomerConfirmation.html" + "?orderId=" + orderList[i].orderNum +"&serviceType=1"+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
										break;
									
								}
								break;
							case 2:
								products[j]["servieType"] = "换货";
								switch (products[j].isCheck) {
									
									case 1:
										products[j]["verifyStatus"] = "审核中";
										products[j]["toStatusUrl"] = "./service-verify-verifying.html" + "?orderId=" + orderList[i].orderNum+"&serviceType=2"+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
										break;
									case 2://审核通过
										products[j]["verifyStatus"] = "审核通过";
										products[j]["toStatusUrl"] = "./service-verify-passed.html" + "?orderId=" + orderList[i].orderNum +"&serviceType=2"+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
										break;
									case 3:
										products[j]["verifyStatus"] = "换货中";
										products[j]["toStatusUrl"] = "./productReturn.html" + "?orderId=" + orderList[i].orderNum+"&serviceType=2"+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
										break;
									case 4:
										products[j]["verifyStatus"] = "客服已发货";
										products[j]["toStatusUrl"] = "./return-exchange-finish.html" + "?orderId=" + orderList[i].orderNum+"&serviceType=2"+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
										break;
									case 5:
										products[j]["verifyStatus"] = "拒绝换货";
										products[j]["toStatusUrl"] = "./service-verify-refusal.html" + "?orderId=" + orderList[i].orderNum+"&serviceType=2"+"&userId=" + orderList[i].userId+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
										break;
									case 6:
										products[j]["verifyStatus"] = "换货完成";
										products[j]["toStatusUrl"] = "./CustomerConfirmation.html" + "?orderId=" + orderList[i].orderNum+"&serviceType=2"+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
										break;
									
								}
								break;
						}


					}	
				}
				orderRow.showOrderRow(orderList);
				orderRow.bindPager(new PagerView('noticePager', data.pageCount, data.page));
				EventBus.clearBind('noticePager' + '-jumpTo', function(e) {
					me.showOrderList(nodeId, e.targetPage);
				});
			} else {
				$(nodeId).html(data.msg);
			}
		});
	}

	/** 订单详细*/
	MagicController.prototype.showOrderContent = function(statusNodeId, contentNodeId, logisticsNodeId, proPattenNodeIds, orderId) {

		var me = this;
		var dateParser = new DateParser();
		var orderDetails = new OrderDetails(statusNodeId, contentNodeId, logisticsNodeId);
		me.magicService.getOrderStatus(orderId, function(data) {

			if (data.ret == 1) {
				var myCreateTime;
				var myClockTime;
				var statusName;
				//订单状态
				for (var i = 0; i < data.statusList.length; i++) {
					var className = "";
					var myTime = data.statusList[i].statusTime;
					switch (i) {
						// case 4:
						// 	className = "wait-receipt"
						// 	break;
						case 3:
							className = "order-complete"
							break;
						case 2:
							className = "ship-goods"
							break;
						case 1:
							className = "payment-success"
							break;
						default:
							className = "submit-order"
							break;
					}
					data.statusList[i]["createTime"] = dateParser.parseToYMD(myTime);
					data.statusList[i]["clockTime"] = dateParser.parseToHM(myTime);
					data.statusList[i]["className"] = className;
					if (i == data.statusList.length - 1) {
						data.statusList[i]["pointTag"] = "true"
						statusName = data.statusList[i].status;
					}
				};
				data["orderNum"] = orderId;

				data["statusName"] = statusName !== null ? statusName : "暂无";
				// console.log(data)
				orderDetails.showOrderStatus(data);
				// orderDetails.showOrderLogistics(data.logisticsDetail);
			} else {
				$(statusNodeId).html(data.msg).css("color", "red");
			}
		});
		me.magicService.getOrderContent(orderId, function(data) {
			var DELIVER_TYPE_WEEK = "只工作日送货（双休日、假日不用送）",
				DELIVER_TYPE_WORK = "工作日送货（适合于办公地址）",
				DELIVER_TYPE_ALL = "双休日、假日送货（适合于家庭地址）";
			var PAY_TYPE_ONLINE = "在线支付",
				PAY_TYPE_OFFINE = "货到付款";
			var STATUS_WAIT = "等待付款",
				STATUS_FAIL = "付款失败",
				STATUS_SUCCESS = "付款成功";
			if (data.ret == 1) {
				// console.log(data);
				//商品详情
				switch (data.order.deliveryTimeType) {

					case 1:
						deliverName = DELIVER_TYPE_ALL
						break;
					case 2:
						deliverName = DELIVER_TYPE_WORK
						break;
					default:
						deliverName = DELIVER_TYPE_WEEK
						break;
				}
				switch (data.order.payType) {

					case 2:
						myPayTypeName = PAY_TYPE_OFFINE
						break;
					default:
						myPayTypeName = PAY_TYPE_ONLINE
						break;
				}
				data["payTypeName"] = myPayTypeName;
				data["deliveryTimeTypeName"] = deliverName;
				var invoiceTypeName = "";
				if (data.invoice != null) {
					var INVOICE_TYPE_PESONAL = "个人",
						INVOICE_TYPE_COM = "单位";
					switch (data.invoice.invoiceType) {
						case 2:
							invoiceTypeName = INVOICE_TYPE_COM
							break;
						default:
							invoiceTypeName = INVOICE_TYPE_PESONAL
							break;
					}
					data.invoice["invoiceTypeName"] = invoiceTypeName;
				}
				// data.address.detail ="";
				//商品清单
				// var productList = new Array();
				// for (var i = 0; i < data.orderItems.length; i++) {
				// 	productList.push(ProductRepo.findByProductId(data.orderItems[i].productId));
				// };
				var orderItems = data.orderItems;
				var products = data.products;
				var sumAmount = 0;
				var totalNum = 0;
				var freight = 0;
				if (data.order !== null) {
					sumAmount = data.order.sumAmount;
					if (data.order.type === 3) {
				// 免费腕带活动 运费15元
						freight = "15.00";
					};
					data["freight"] = freight
				}
				if (orderItems.length > 0) {

					$(proPattenNodeIds.orderTable).html("");
					for (i in products) {
						userbuyinfo = products[i];
						// console.log(userbuyinfo);
						totalNum += orderItems[i].num;
						$(proPattenNodeIds.orderTable).append('<tr><td>' + userbuyinfo.name + '</td><td>' + orderItems[i].price + '</td><td>' + orderItems[i].num + '</td><td>' + orderItems[i].num * orderItems[i].price + '</td></tr>');
						$(proPattenNodeIds.amount).html(totalNum);
					}
					$(proPattenNodeIds.totalPrice).html(sumAmount);
				}
				data["userbuyinfo"] = userbuyinfo;
				orderDetails.showOrderContent(data);
			} else {
				$(contentNodeId).html(data.msg).css("color", "red");
			}
		});


		//物流信息
		me.magicService.getLogisticsInfo(orderId, function(data) {
			if (data.ret == 1) {
				if (data.url != '') {
					document.getElementById("kuaidi100").src = data.url;
				} else {
					orderDetails.showOrderLogistics(data.logisticsInfo);
				}
			} else {
				$(logisticsNodeId).html(data.msg).css("color", "red");
			}
		});
	};

	MagicController.prototype.survey = function(params) {
		var me = this;
		me.magicService.survey(params, function(data) {
			if (data.ret == 1) {
				me.showAlertDialog({
					title: "温馨提示",
					content: "<h3 style='color:#74a051;text-align:center'>√提交成功！</h3><p>感谢您的参与，中奖名单将在微博中公布<br>请关注<strong style='color:#eb1e67'>乐跑手环官方微博</strong> <br><a href='http://www.weibo.com/3820258740'  target='_blank'>http://www.weibo.com/3820258740 </a>了解最新动态！</p>",
					positiveText: "回到官网"
				}, function() {
					location.href = magicConstant.targetUrl.indexPage;
				});
			} else {
				me.showAlertDialog(data.msg);
			}
		})
	};

	MagicController.prototype.resendEmail = function(account) {
		var me = this;
		me.magicService.resendEmail(account, function(data) {
			if (data.ret == 1) {
				me.sendEmailRepo(account, "注册");
			} else {
				me.showAlertDialog(data.msg);
			}
		});
	};


	MagicController.prototype.showProductReturnApplication = function(nodeId_exchange,nodeId_return,nameId,emailId,ordId,provinceId,cityId,countryId,streetId,postalCodeId,mobileId,telephoneId,orderId,callback) {
		var me = this;
		var productRow = new ProductROW(nodeId_exchange,nodeId_return);
		me.magicService.createReturnExchangeForm(orderId,function(data) {
			if (data.ret == 1) {
				var defaultAddress = data.address;
				var userInfo = data.user;
				var productReturnList = data.productReturnList;
				var defaultcoreNum = [];
				var defaultUsbNum = [];
				var defaultProductNum = [];

				$(nameId).val(defaultAddress.realName);
				$(emailId).val(userInfo.email);
				$(ordId).val(orderId);
				$(provinceId).val(defaultAddress.province);
				$(cityId).val(defaultAddress.city);
				$(countryId).val(defaultAddress.country);
				$(streetId).val(defaultAddress.street);
				$(postalCode).val(defaultAddress.postalCode);
				$(mobileId).val(defaultAddress.mobile);
				$(telephoneId).val(defaultAddress.telephone);
				for(i in productReturnList){
					if(productReturnList[i].categoryId == '2'||productReturnList[i].categoryId =='5'){
						productReturnList[i].categoryId = true;
					}else{
						productReturnList[i].categoryId = false;
					}

					//商品展示标识处理
					if(productReturnList[i].isCheck == '0' || productReturnList[i].isCheck == '5'){
						productReturnList[i].checkStatus = 0;
					}else{
						productReturnList[i].checkStatus = 1;
					}
					var blueNum = 0,
					blackNum = 0,
					greenNum = 0,
					redNum = 0,
				    yellowNum = 0,
				    purpleNum = 0;
					var productId = productReturnList[i].productId;
					switch(productId){
						case 1019:
							blueNum = productReturnList[i].num;
							break;
						case 1020:
							blackNum = productReturnList[i].num;
							break;
						case 1021:
							greenNum = productReturnList[i].num;
							break;
						case 1022:
							redNum = productReturnList[i].num;
							break;
						case 1023:
							yellowNum = productReturnList[i].num;
							break;
						case 1024:
							purpleNum = productReturnList[i].num;
							break;
						case 1011:
							yellowNum = productReturnList[i].num;
							break;
						case 1010:
							redNum = productReturnList[i].num;
							break;
						case 1009:
							greenNum = productReturnList[i].num;
							break;
						case 1008:
							blackNum = productReturnList[i].num;
							break;
						case 1007:
							blueNum = productReturnList[i].num;
							break;
						case 1012:
							purpleNum = productReturnList[i].num;
							break;
								

					}

					productReturnList[i]['blueNum'] = blueNum;
					productReturnList[i]['blackNum'] = blackNum;
					productReturnList[i]['greenNum'] = greenNum;
					productReturnList[i]['redNum'] = redNum;
					productReturnList[i]['yellowNum'] = yellowNum; 
					productReturnList[i]['purpleNum'] = purpleNum;

					defaultcoreNum.push(productReturnList[i].coreNum);	
					defaultUsbNum.push(productReturnList[i].coreNum);	
					defaultProductNum.push(productReturnList[i].num);	
				}

				productRow.showProductRow(data);
				// console.log(defaultcoreNum,defaultUsbNum,defaultProductNum)
				callback();	
			}else {
				me.showAlertDialog(data.msg);
			}
		});
	};

	MagicController.prototype.submitProductReturnApplication =function(returnExchangeInfo,returnId,type){
		var me = this;
		var orderId = returnExchangeInfo[1],
			userId = returnExchangeInfo[0],
			returnReason = returnExchangeInfo[2],
			buyChannel = parseInt(returnExchangeInfo[3]),
			serviceType = returnExchangeInfo[4],
			image = returnExchangeInfo[5],
			itemjson = JSON.stringify(returnExchangeInfo[6]),
			pids = returnExchangeInfo[7];
			//console.log(orderId,returnReason,buyChannel,serviceType,image,itemjson);
		me.magicService.submitReturnExchangeProduct(orderId,returnReason,buyChannel,serviceType,image,itemjson,returnId,
		function(data) {
			if (data.ret == 1) {
				var serviceType = data.serviceType;
				var returnId = data.returnId;
				location.href = "return-exchange-application-confirm.html"+"?serviceType="+serviceType+"&returnId="+returnId+"&orderId="+orderId+"&userId="+userId+"&itemjson="+itemjson+"&pids="+pids+"&type="+type;
			} else {
				me.showAlertDialog(data.msg);
			}
		});
	};

	MagicController.prototype.showConfirmApplicationInfo = function(nodeId,returnId,serviceType,callback){
		var me = this;
		var dateParser = new DateParser();
		var applicationInfo = new ApplicationInfo(nodeId);
		me.magicService.confirmApplicationFormInfo(returnId,serviceType,function(data){
			var productReturn = data.productReturn;	
			var productReturnItemList = data.productReturnItemList;
			var address = data.address;
			var addressStr = address.province + address.city + address.country + address.street;
			address.addressStr = addressStr;
			if(productReturn.buyChannel == 1){
				productReturn.buyChannelStr = "乐跑官网";
			}else if(productReturn.buyChannel == 2){
				productReturn.buyChannelStr = "腾讯爱逛";
			}
			// 退换货类型
			if(productReturn.serviceType == 2){
				productReturn.serviceTypeStatus = 1;
			}else{
				productReturn.serviceTypeStatus = 0;
			}
			//处理日期
			var createTime = data.productReturn.createTime;
			data.productReturn.toCreateTime = dateParser.parseToYMD(createTime);

			//处理图片链接
			var image = data.productReturn.image;
			imageArray = image.split(',');
			var imageUrlArray = [];
			for(var j=0;j<imageArray.length;j++){
				imageUrlArray.push({simage:imageArray[j]});
			}
			data.productReturn.imageUrlArray = imageUrlArray;
			
			//删除重复商品  并将数量相加
			for(var i=0;i<productReturnItemList.length-1;i++){
				for(var j=i+1;j<productReturnItemList.length;j++){
					if(productReturnItemList[i].productId === productReturnItemList[j].productId){
						var baseNum = productReturnItemList[i].num;
						var repeatNum = productReturnItemList[j].num;
						productReturnItemList[i].num = baseNum + repeatNum;
						productReturnItemList.splice(j,1);
						j--;
					}
				}
			};
			
			for(i in productReturnItemList){
				var productId = parseInt(productReturnItemList[i].productId);
				switch(productId){
					case 1023:
						productReturnItemList[i].productName = "乐跑手环(柠檬黄)，包含机芯和USB充电器";
						break;
					case 1022:
						productReturnItemList[i].productName = "乐跑手环(玫瑰红)，包含机芯和USB充电器";
						break;
					case 1021:
						productReturnItemList[i].productName = "乐跑手环(浅草绿)，包含机芯和USB充电器";
						break;
					case 1020:
						productReturnItemList[i].productName = "乐跑手环(玄墨黑)，包含机芯和USB充电器";
						break;
					case 1019:
						productReturnItemList[i].productName = "乐跑手环(海洋蓝)，包含机芯和USB充电器";
						break;
					case 1024:
						productReturnItemList[i].productName = "乐跑手环(樱桃紫)，包含机芯和USB充电器";
						break;
					case 1011:
						productReturnItemList[i].productName = "腕带(柠檬黄)";
						break;
					case 1010:
						productReturnItemList[i].productName = "腕带(玫瑰红)";
						break;
					case 1009:
						productReturnItemList[i].productName = "腕带(浅草绿)";
						break;
					case 1008:
						productReturnItemList[i].productName = "腕带(玄墨黑)";
						break;
					case 1007:
						productReturnItemList[i].productName = "腕带(海洋蓝)";
						break;
					case 1012:
						productReturnItemList[i].productName = "腕带(樱桃紫)";
						break;
					case 1025:
						productReturnItemList[i].productName = "机芯";
						break;
					case 1027:
						productReturnItemList[i].productName = "USB充电器";
						break;
				}
			}
			
			applicationInfo.showApplicationInfo(data);
			callback();
			
		})
	};

	MagicController.prototype.confirmSubmitApplicationForm = function(returnId,serviceType,pids,type){
		var me = this;
		me.magicService.confirmSubmitApplicationForm(returnId,serviceType,pids,function(data){
			if(data.ret == 1){
				if(parseInt(type)==1){
					location.href = magicConstant.targetUrl.myOrder;
				}else{
					location.href = "return-exchange-application-aiguang.html";
				}
			}else{
				me.showAlertDialog(data.msg);
			}

		})
	}

	MagicController.prototype.showProductReturnStatus =function(nodeId,returnId,serviceType,callback){
		var me = this;
		var verifyStatusInfo = new VerifyStatusInfo(nodeId);
		var serviceType = parseInt(serviceType);
		// console.log(returnId,serviceType)
		me.magicService.getProductReturnStatus(returnId,serviceType,function(data){
			if (data.ret == 1) {
				
				var productReturn = data.productReturn;
				var productItemList = data.productItemList;
				var address = data.address;
				var addressStr = address.province + address.city + address.country + address.street;
				address.addressStr = addressStr;
				if(productReturn.buyChannel == 1){
					productReturn.buyChannelStr = "乐跑官网";
				}else if(productReturn.buyChannel == 2){
					productReturn.buyChannelStr = "腾讯爱逛";
				}
				// 退换货类型
				if(productReturn.serviceType == 2){
					productReturn.serviceTypeStatus = 1;
				}else{
					productReturn.serviceTypeStatus = 0;
				}

				//插入拒绝退还理由
				if(productReturn.rejectReason){
					var containerDom = $('#verify_status');
					var rejectDom = "<span class='rejectReason'>"+productReturn.rejectReason+"</span>";
					containerDom.append(rejectDom);
				}

				//处理图片链接
				var image = data.productReturn.image;
				imageArray = image.split(',');
				var imageUrlArray = [];
				for(var j=0;j<imageArray.length;j++){
					imageUrlArray.push({simage:imageArray[j]});
				}
				data.productReturn.imageUrlArray = imageUrlArray;
				
				//删除重复商品  并将数量相加
				for(var i=0;i<productItemList.length-1;i++){
					for(var j=i+1;j<productItemList.length;j++){
						if(productItemList[i].product.id === productItemList[j].product.id){
							var baseNum = productItemList[i].num;
							var repeatNum = productItemList[j].num;
							productItemList[i].num = baseNum + repeatNum ;
							productItemList.splice(j,1);
							j--;
						}
					}
				};

				for(i in productItemList){
					var productId = parseInt(productItemList[i].product.id);
					switch(productId){
						case 1023:
							productItemList[i].productName = "乐跑手环(柠檬黄)，包含机芯和USB充电器";
							break;
						case 1022:
							productItemList[i].productName = "乐跑手环(玫瑰红)，包含机芯和USB充电器";
							break;
						case 1021:
							productItemList[i].productName = "乐跑手环(浅草绿)，包含机芯和USB充电器";
							break;
						case 1020:
							productItemList[i].productName = "乐跑手环(玄墨黑)，包含机芯和USB充电器";
							break;
						case 1019:
							productItemList[i].productName = "乐跑手环(海洋蓝)，包含机芯和USB充电器";
							break;
						case 1024:
							productItemList[i].productName = "乐跑手环(樱桃紫)，包含机芯和USB充电器";
							break;
						case 1011:
							productItemList[i].productName = "腕带(柠檬黄)";
							break;
						case 1010:
							productItemList[i].productName = "腕带(玫瑰红)";
							break;
						case 1009:
							productItemList[i].productName = "腕带(浅草绿)";
							break;
						case 1008:
							productItemList[i].productName = "腕带(玄墨黑)";
							break;
						case 1007:
							productItemList[i].productName = "腕带(海洋蓝)";
							break;
						case 1012:
							productItemList[i].productName = "腕带(樱桃紫)";
							break;
						case 1025:
							productItemList[i].productName = "机芯";
							break;
						case 1027:
							productItemList[i].productName = "USB充电器";
							break;
					}
				}
				verifyStatusInfo.showVerifyStatusInfo(data);
				callback();
				
			} else {
				me.showAlertDialog(data.msg);
			}
		})
	};

	MagicController.prototype.fillLogisticsInfo =function(nodeId,returnId,serviceType,callback){
		var me = this;
		me.magicService.fillLogisticsInfo(returnId,serviceType,function(data){
			if (data.ret == 1) {
				var expressCompaniesList = data.expressCompaniesList;
				var htmlstr ='';
				for(i in expressCompaniesList){
					htmlstr += '<option value="' + expressCompaniesList[i].id + '">'+ expressCompaniesList[i].name + '快递</option>';
				}
				$(nodeId).html(htmlstr);
			}else{
				me.showAlertDialog(data.msg);
			}
		});
		callback();
	};

	MagicController.prototype.submitLogisticsInfo =function(returnId,expressId,expressNo,orderId,serviceType,pids,refundAccount,type,callback){
		var me = this;
		me.magicService.submitLogisticsInfo(returnId,expressId,expressNo,orderId,pids,refundAccount,function(data){
			if (data.ret == 1) {
				location.href = "productReturn.html"+"?orderId=" + orderId +"&serviceType=" + serviceType + "&type=" + type;
			}else{
				me.showAlertDialog(data.msg);
			}
		});
	};

	MagicController.prototype.showLogisticsTrackInfo =function(returnId,logisticsNodeId){
		var me = this;
		me.magicService.getLogisticsTrackInfo(returnId,function(data){
			if (data.ret == 1) {
				if (data.trackURL != '') {
					document.getElementById("kuaidi100").src = data.trackURL;
					//document.getElementById("kuaidi100").src = "http://www.kuaidi100.com/kuaidiresult?id=5407464";
				} else {
					$(logisticsNodeId).html("暂无物流信息").css("color", "red");
				}
			} else {
				me.showAlertDialog(data.msg);
				
			}
		});
	};

	MagicController.prototype.customerConfirmComplete =function(returnId,orderId,serviceType,type){
		var me = this;
		me.magicService.createProductReturnComplete(returnId,orderId,function(data){
			if (data.ret == 1) {
				location.href = magicConstant.targetUrl.CustomerConfirmation+"?serviceType=" + serviceType + "&type=" + type;
			} else {
				me.showAlertDialog(data.msg);
				
			}
		});
	};

	MagicController.prototype.getOrderListFromAiguangByUserId =function(nodeIdRow,nodeIdUl,callback){
		var me = this;
		var aiguangOrderRow = new AiguangOrderRow(nodeIdRow);
		var dateParser = new DateParser();
		me.magicService.getOrderListFromAiguangByUserId(function(data){
			if (data.ret == 1) {
				var orderList = data.orderList;
				if(orderList.length){
					$(nodeIdUl).empty();
					for(i in orderList){
						$(nodeIdUl).append('<li><input type="radio" value="'+orderList[i].orderNum+'" name="orderId" disabled="disabled"/><span class="aiguang_orderContext">'+orderList[i].orderNum+'</span></li>');
						if(orderList[i].status == 2){
							//function productReturnExchangeEntry(index){
								//var i = index;
								//orderList[i].payTime = new Date().getTime();
								//判断退换货过期时间
								var time = orderList[i].payTime + 1728000000;//默认付款后5天收货+15天=20天
								var today = new Date().getTime();
								if(today>time){//过期
									orderList[i].expire = 1;
								}

								//退换货权利判断
								if(!orderList[i].expire){
									orderList[i].productReturnExchangeRight = 1;
								}

								//被退换商品id
								var products = orderList[i].products;
								var pidArray = [];
								for(k in products){
									if(products[k].isReturn){
										pidArray.push(products[k].id);
									}	
								}
								orderList[i].pids = pidArray.join(',');

								//处理支付时间格式
								orderList[i]["payTimeStr"] = dateParser.parseToYMD(orderList[i].payTime);
								
								for(j in products ){
									var servieType = "";
									var verifyStatus = "";
									var toStatusUrl = "javascript:void(0)";
									switch (products[j].isReturn) {
										case 0:
											products[j]["servieType"] = "退货/换货";
											products[j]["toStatusUrl"] = "return-exchange-application.html" + "?orderId=" + orderList[i].orderNum + "&userId=" + orderList[i].userId + "&type=" + orderList[i].type;
											break;
										case 1:
											products[j]["servieType"] = "退货";
											switch (products[j].isCheck) {
											
												case 1:
													products[j]["verifyStatus"] = "审核中";
													products[j]["toStatusUrl"] = "./service-verify-verifying.html" + "?orderId=" + orderList[i].orderNum +"&serviceType=1"+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
													break;
												case 2://审核通过
													products[j]["verifyStatus"] = "审核通过";
													products[j]["toStatusUrl"] = "./service-verify-passed.html" + "?orderId=" + orderList[i].orderNum +"&serviceType=1"+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
													break;
												case 3:
													products[j]["verifyStatus"] = "退货中";
													products[j]["toStatusUrl"] = "./productReturn.html" + "?orderId=" + orderList[i].orderNum+"&serviceType=1"+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
													break;
												case 4:
													products[j]["verifyStatus"] = "确认已退款";
													products[j]["toStatusUrl"] = "./return-exchange-finish.html" + "?orderId=" + orderList[i].orderNum +"&serviceType=1"+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
													break;
												case 5:
													products[j]["verifyStatus"] = "拒绝退货";
													products[j]["toStatusUrl"] = "./service-verify-refusal.html" + "?orderId=" + orderList[i].orderNum +"&serviceType=1" + "&userId=" + orderList[i].userId+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
													break;
												case 6:
													products[j]["verifyStatus"] = "退货完成";
													products[j]["toStatusUrl"] = "./CustomerConfirmation.html" + "?orderId=" + orderList[i].orderNum +"&serviceType=1"+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
													break;
												
											}
											break;
										case 2:
											products[j]["servieType"] = "换货";
											switch (products[j].isCheck) {
												
												case 1:
													products[j]["verifyStatus"] = "审核中";
													products[j]["toStatusUrl"] = "./service-verify-verifying.html" + "?orderId=" + orderList[i].orderNum+"&serviceType=2"+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
													break;
												case 2://审核通过
													products[j]["verifyStatus"] = "审核通过";
													products[j]["toStatusUrl"] = "./service-verify-passed.html" + "?orderId=" + orderList[i].orderNum +"&serviceType=2"+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
													break;
												case 3:
													products[j]["verifyStatus"] = "换货中";
													products[j]["toStatusUrl"] = "./productReturn.html" + "?orderId=" + orderList[i].orderNum+"&serviceType=2"+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
													break;
												case 4:
													products[j]["verifyStatus"] = "客服已发货";
													products[j]["toStatusUrl"] = "./return-exchange-finish.html" + "?orderId=" + orderList[i].orderNum+"&serviceType=2"+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
													break;
												case 5:
													products[j]["verifyStatus"] = "拒绝换货";
													products[j]["toStatusUrl"] = "./service-verify-refusal.html" + "?orderId=" + orderList[i].orderNum+"&serviceType=2"+"&userId=" + orderList[i].userId+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
													break;
												case 6:
													products[j]["verifyStatus"] = "换货完成";
													products[j]["toStatusUrl"] = "./CustomerConfirmation.html" + "?orderId=" + orderList[i].orderNum+"&serviceType=2"+"&returnId="+products[j].returnId + "&pids=" + orderList[i].pids + "&type=" + orderList[i].type;
													break;
											}
											break;
									}
								}	
							//}
							aiguangOrderRow.showAiguangOrderInfo(orderList);
						}else{
							me.showAlertDialog("此订单您还没有付款，无申请退换资格");
						}

					}

				}
			} else {
				me.showAlertDialog(data.msg);
				
			}
		});
	};

	MagicController.prototype.orderNumVerification =function(orderId,buyerName,nodeId1,nodeId2,callback){
		var me = this;
		me.magicService.getOrderInfoByExternalId(orderId,buyerName,function(data){
			if (data.ret == 1) {
				callback(data);
				$(nodeId1).show();
				$(nodeId2).remove();
			} else {
				me.showAlertDialog(data.msg);
				$(nodeId1).show();
				$(nodeId2).remove();
			}
		});
	};


});