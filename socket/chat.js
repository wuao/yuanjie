var sessionHandle = require( root + '/middlewares/before').session;
var User = require( root + '/model/userModel').User;
var authUsers = {};

function sessionAuth (socket, next) {
    var req=socket.handshake;
    var cookie = socket.handshake.query.cookie;
    if (cookie) {
        req.headers.cookie = cookie
    }

    req.originalUrl='/';
    sessionHandle(req, {}, next);
}

function userAuth (socket, next) {
    var userId = socket.handshake.session.user_id;
    if (userId) {
        User.findOne({_id: userId}, function (err, user) {
            if (err || !user) return socket.emit('ERROR', {error: 'auth error'});
            
            socket.user = user;
            authUsers[user._id] = {
                socket: socket,
                user: user
            };
            socket.emit('SUCCESS', { success: 'auth success'} );

            return next();
        })
    }else {
        return socket.emit('ERROR', {error: 'auth error'});
    }
}

function connection (socket) {
    socket.on('chat', onChat.bind(socket));
    socket.on('disconnect', disConnection.bind(socket));
}

function onChat (message) {
    if (!this.user && !!authUsers[this.user._id]) this.emit('ERROR', 'not auth user');

    var pairUser = authUsers[this.pairUser._id];
    
    if ( !pairUser ) return this.emit('ERROR', {error: 'the pair user is not online'});
    
    //prepare message for send
    var pairMessage = {self: false, user: this.user, message: message, time: Math.round(new Date().getTime()/1000) };
    var message = {self: true, user: this.user, message: message, time: Math.round(new Date().getTime()/1000) };

    // send message to web and ios
    pairUser.socket.emit('chat', pairMessage);
    this.emit('chat', message);

    //support for the fucking shit android chinese character!
    pairUser.socket.emit( 'chat-android', new Buffer(JSON.stringify(pairMessage)).toString('base64') );
    this.emit( 'chat-android', new Buffer(JSON.stringify(message)).toString('base64') );
}

function pairUser (socket, next) {
    var gender = '';

    if (socket.user.gender === 'male') {
        gender = 'female';
    }else {
        gender = 'male';
    }

    var number = getSpecifyNumber(socket.user.number) || socket.user.number;

    User.findOne({number: number, gender: gender}, function (err, user) {
        if (err || !user) return socket.emit('ERROR', {error: 'the pair user not exist'});
        
        socket.pairUser = user;
        next();
    })
}

function getSpecifyNumber (number) {
    switch (number) {
        case 520:
            return 521;
            break;
        case 521:
            return 520;
            break;
        case 13:
            return 14;
            break;
        case 14:
            return 13;
            break;
        default:
            return false;
            break;
    }
}

function disConnection () {
    delete authUsers[this.user._id];
}

module.exports = function (io) {
    io.use(sessionAuth);
    io.use(userAuth);
    io.use(pairUser);
    io.on('connection', connection);
}