var stooge = {
    "first-name": "Jerome",
    "last-name": "Howard"
};

Object.create = function(o) {
    var F = function() {};
    F.prototype = o;
    return new F();
};

var another_stooge = Object.create(stooge);
debugger
stooge.profession = 'actor';
another_stooge.profession    // 'actor'
