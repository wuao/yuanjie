global.root = __dirname;

var config = require('./config/config.js');
var express = require('express');
var app = express();

// before middlewares
require('./middlewares/before.js')(app);

// routes
require('./routes/index')(app);

//after middlewares
require('./middlewares/after.js')(app);

module.exports = app;
