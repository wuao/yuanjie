var User = require( root + '/model/userModel').User;
var express = require('express');
var router = express.Router();
var userLoginValidate = require('./user').userLoginValidate;
var Post = require( root + '/model/postModel').Post;
/* GET home page. */

router.get('/', function(req, res) {
    res.render('index');
});

router.get('/download', function(req, res) {
    res.render('app_down');
});

router.get('/detail', function(req, res) {
    res.render('yuanjie_content');
});

router.get('/news', function(req, res) {
    res.render('news');
});

router.get('/zone', userLoginValidate, function(req, res) {
    Post.find({}).sort({create_at: -1}).populate('_creator comments._creator').exec(function (err, posts) {
        return res.render('circle/circle', { posts: posts });
    })
});

router.get('/write_article', userLoginValidate, function (req, res) {
   res.render('circle/write_article');
});

router.get('/chat', userLoginValidate, pairUser, function (req, res) {
    res.render('circle/love_chart', { pairUser: req.pairUser});
});

router.post('/chat', userLoginValidate, pairUser, function (req, res) {
    res.render('circle/love_chart', { pairUser: req.pairUser});
});

function pairUser (req, res, next) {
    var gender = '';
    if (req.user.gender === 'male'){
        gender = 'female';
    }else{
        gender = 'male';
    }

    var number = getSpecifyNumber(req.user.number) || req.user.number;

    User.findOne({number: number, gender: gender}, function (err, user) {
        req.pairUser = user;
        next();
    })
}

function getSpecifyNumber (number) {
    switch (number) {
        case 520:
            return 521;
            break;
        case 521:
            return 520;
            break;
        case 13:
            return 14;
            break;
        case 14:
            return 13;
            break;
        default:
            return false;
            break;
    }
}

module.exports.router = router;
