var Post = require(root + '/model/postModel').Post;
var express = require('express');
var userLoginValidate = require('./user').userLoginValidate;
var router = express.Router();
var multer = require('multer');
var async = require('async');
var Imager = require('imager');
var imagerConfig = require(root + '/config/imager');
var imager = new Imager(imagerConfig, 'Local')

router.get('/list', function(req, res) {
    if (!req.param('page') && req.param('limit')) return res.send(403, 'expect params');
    var page = req.param('page');
    var limit = req.param('limit');

    Post.find({}).sort({
        create_at: -1
    }).populate('_creator comments._creator')
    .skip((page - 1) * limit)
    .limit(limit)
    .exec(function(err, posts) {
        return res.send(posts);
    })
})

// need user login
router.use(userLoginValidate);

router.post('/', multer({
    dest: root + '/uploads/'
}), check, uploadImages, function(req, res) {
    Post.create({
        _creator: req.user._id,
        content: req.body.content,
        images: req.images
    }, function(err, post) {
        if (err) return res.render('index', {
            error: err
        });

        return res.redirect('/zone', {
            post: post
        });
    })
});

function uploadImages(req, res, next) {
    if (Object.keys(req.files).length !== 0) {
        imager.upload(req.files.images, function(err, cdn, files) {
            req.images = files;
            next();
        }, 'items');
    } else {
        req.images = req.body.images || [];
        next();
    }
}

router.post('/comment', check, function(req, res) {
    Post.findOne({
        _id: req.body.id
    })
        .populate('_creator comments._creator')
        .exec(function(err, post) {
            var comment = {
                _creator: req.user._id,
                content: req.body.content
            };

            post.comments.push(comment);

            post.save(function(err) {
                Post.find({}).sort({
                    creat_at: -1
                }).populate('_creator comments._creator').exec(function(err, posts) {
                    return res.redirect('/zone', {content: comment.content});
                })
            })
        })
});

function check(req, res, next) {
    req.checkBody('content', '发布内容不能为空');

    var errors = req.validationErrors();
    if (errors) {
        res.render('index', {
            error: '填写的内容不能为空'
        });
    } else {
        next();
    }
}

module.exports.router = router;