var User = require(root + '/model/userModel.js').User;
var express = require('express');
var router = express.Router();
var multer = require('multer');
var Imager = require('imager');
var imagerConfig = require(root + '/config/imager');
var imager = new Imager(imagerConfig, 'Local')
var async = require('async');

// //without user check

router.get('/:username', function (req, res) {
    User.findOne({username: req.params.username}, function (err, user) {
        if (!user) return res.render('circle/circle', {error: '该用户不存在!'});
        res.render('circle/see_personal', {theUser: user});
    })
})

//register
router.post('/', createCheck, function(req, res) {
    User.create(req.body, function(err, user) {
        if (err && err.errors.username && err.errors.username.message === 'Error, expected username to be unique.') {
            return res.render('index', {
                error: '用户名已存在，请重新注册！'
            });
        } else if (err) {
            return res.render('index', {
                error: err
            });
        }

        req.session.user_id = user._id;
        req.session.save(function(err) {
            res.render('index', {
                user: user
            });
        })
    });
});

// login
router.post('/login', function(req, res) {
    if (!req.body.username) return res.render('index', {
        error: '缺少用户名'
    });
    if (!req.body.password) return res.render('index', {
        error: '缺少密码'
    });

    User.findOne({
        username: req.body.username
    }, function(err, user) {
        if (err) return res.render('index', {
            error: err
        });
        if (!user) return res.render('index', {
            error: '没有此用户'
        });
        if (!user.authenticate(req.body.password)) return res.render('index', {
            error: '密码错误'
        });

        req.session.user_id = user._id;
        req.session.save(function(err) {
            return res.redirect('/', {
                user: user
            });
        })
    })
})

// check user middleware
router.use(userLoginValidate);

// // need user check routers

router.get('/session/logout', function(req, res) {
    req.session.destroy(function(err) {
        res.redirect('/');
    })
})

router.get('/', function(req, res) {
    res.render('circle/modify_personal', {
        user: req.user
    });
});

//change user info and avatar and password
router.post('/modify', multer({
    dest: root + '/uploads/'
}), function(req, res) {
    delete req.body.username;
    delete req.body.number;
    debugger
    User.findOneAndUpdate({
        _id: req.user._id
    }, req.body, function(err, user) {

        if (err) console.log(err);
        async.series([
            // not equal, mean use change their password
            function(n) {
                if (req.body.password && !user.authenticate(req.body.password) && req.body.password.trim() !== '') {
                    user.password = req.body.password;
                }
                n();
            },
            // if user upload avatar
            function(n) {
                if (Object.keys(req.files).length !== 0) {
                    imager.upload([req.files.avatar], function(err, cdn, files) {
                        user.avatar = files[0];
                        n();
                    }, 'items')
                } else {
                    n();
                }
            },
            //save user
            function(n) {
                user.save(function(err) {
                    if (err) console.log(err);
                    return res.render('circle/modify_personal', {
                        user: user
                    });
                })
            }
        ]);

    })
});

//if is valid, then save the number to user schema
router.post('/pair', checkPair, checkCode, function(req, res) {
    User.findOne({
        _id: req.user._id
    }, function(err, user) {
        user.gender = req.body.gender;
        user.number = Number(req.body.number);
        user.save(function(err, user) {
            return res.render('circle/circle', {
                user: user
            });
        })
    })
})

function userLoginValidate(req, res, next) {
    if (req.user) return next();
    return res.render('index', {
        error: '请先登录！'
    });
}

function createCheck(req, res, next) {
    debugger
    req.checkBody('username', '不能为空').notEmpty();
    req.checkBody('password', '不能为空').notEmpty();
    req.checkBody('nickname', '不能为空').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.render('index', {
            error: '填写的内容不能为空'
        });
    } else {
        next();
    }
}

function checkPair(req, res, next) {
    req.checkBody('number', '不能为空').notEmpty();
    req.checkBody('gender', '不能为空').notEmpty();
    req.checkBody('code', '不能为空').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.render('circle/circle', {
            error: '填写的内容不能为空'
        });
    } else {
        next();
    }
}

function checkCode(req, res, next) {
    var number = req.body.number;
    var gender = req.body.gender;
    var code = req.body.code;

    if (gender !== 'male' && gender !== 'female') return res.render('index', {
        error: 'error'
    });

    var parse = require(root + '/util/ringCodeGenerate').parse;

    parse(root + '/files/' + gender + '.txt', function(err, data) {
        var ok = false;
        data.forEach(function(v) {
            if (code === v.code && gender === v.gender && Number(number) === v.number) ok = true;
        });

        if (!ok) {
            return res.render('circle/circle', {
                error: '戒指信息错误，请检查 戒指号码、性别、配对密码 是否正确'
            });
        } else {
            User.findOne({
                gender: gender,
                number: number
            }, function(err, user) {
                if (err) console.log(err);
                if (user) return res.render('circle/circle', {
                    error: '该戒指已被注册'
                });
                next();
            });
        }

    })
}

function isJsonRequest(req) {
    if (req.headers['content-type'] === 'application/json') return true;
    return false;
}

module.exports.userLoginValidate = userLoginValidate;
module.exports.router = router;