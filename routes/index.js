module.exports = function (app) {
    var index = require('./web/index').router;
    var user = require('./web/user').router;
    var post = require('./web/post').router

    // front page
    app.use('/',index);

    //user    
    app.use('/user', user);

    //post
    app.use('/post', post);

    //apis
    app.use( '/file', require('./api/file').router );
}