var env = process.env.NODE_ENV || 'dev';

var settings = {
    db: {
        uri: "mongodb://localhost/yuanjie_" + env,
        session_collection: 'session'
    },
    session_secret: '$86=$j.fs%?%>4A$366*3642',
    imageUploads: root + '/public/files/images/uploads'
}

module.exports = settings;